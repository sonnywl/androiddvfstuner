
package com.lib;

import android.util.Log;

import java.io.File;
import java.text.DecimalFormat;

public class A {
    private A() {
    }

    public static final String PREFERENCE = "Profiler";
    public static final String PREFIX = "VpnProfiler";
    public static final String FREQUENCIES = "Frequencies";
    public static final String GOVERNOR = "Governors";
    public static final String ANALYZE = "Analyze";

    public static final int PROFILING_POLLING_INTERVAL = 150;
    public static final int ANALYSIS_POLLING_INTERVAL = 1000;
    public static final int SECONDS = 5;

    public static final int MEMORY_STORAGE = (SECONDS * 1000) / PROFILING_POLLING_INTERVAL;

    public static final DecimalFormat df = new DecimalFormat("#.##");

    public static String formatDec(double value) {
        return df.format(value);
    }

    public static String toStringArray(int[] intArray) {
        StringBuilder sb = new StringBuilder();
        for (long element : intArray) {
            sb.append(element);
            sb.append(",");
        }
        return sb.toString();
    }

    public static String toStringArray(long[] longArray) {
        StringBuilder sb = new StringBuilder();
        for (long element : longArray) {
            sb.append(element);
            sb.append(",");
        }
        return sb.toString();
    }

    public static String toStringArray(Object[] objectArray) {
        StringBuilder sb = new StringBuilder();
        for (Object element : objectArray) {
            sb.append(element.toString());
            sb.append(",");
        }
        return sb.toString();
    }

    public static void logArray(String tag, Object[] array) {
        for (Object element : array) {
            log(tag, element.toString());
        }
    }

    public static void log(String tag, Object msg) {
        Log.i(tag, msg.toString());
    }

    /**
     * Returns the sudo file path
     * 
     * @return
     */
    public static String getSudo() {
        for (SudoLocations su : SudoLocations.values()) {
            File file = new File(su.getSu());
            if (file.exists()) {
                return su.getSu();
            }
        }
        return "su";
    }

    private static enum SudoLocations {
        XBIN("/system/xbin/su"), BIN("/system/bin/su");

        private String su;

        SudoLocations(String location) {
            su = location;
        }

        public String getSu() {
            return su;
        }
    }
    
}
