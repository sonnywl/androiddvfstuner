
package com.lib;

public class CircularIntArray extends CircularArray {

    final int[] dataSet;

    public CircularIntArray(int size) {
        dataSet = new int[size];
        capacity = size;
    }

    public void add(int data) {
        increment(true);
        dataSet[pointer] = data;
    }

    public double getStdDev() {
        int size = size();
        if (size == 0) {
            return 0;
        }
        return getStdDev(getAverage());
    }
    
    public double getStdDev(double avg) {
        int size = size();
        if (size == 0) {
            return 0;
        }

        double total = 0;
        int loc = pointer;
        for (int i = 0; i < size; i++) {
            if (loc < 0) {
                if (capacityReached) {
                    loc = dataSet.length - 1;
                } else {
                    break;
                }
            }
            total += Math.pow(dataSet[i] - avg, 2);
            loc--;
        }
        if (size() - 1 == 0) {
            return 0;
        }
        return Math.sqrt(total / (size() - 1));
    }

    public double getAverage() {
        double avg = 0;
        long total;
        if (capacityReached) {
            for (int i = 0; i < dataSet.length; i++) {
                avg += dataSet[i];
            }
        } else {
            for (int i = 0; i <= pointer; i++) {
                avg += dataSet[i];
            }
        }
        total = size();
        if (total == 0) {
            total = 1;
        }
        return avg / total;
    }

    public String getLRU() {
        StringBuilder sb = new StringBuilder();
        if (pointer >= 0) {
            int initpointer = pointer;
            do {
                increment(false);
                sb.append(dataSet[pointer]);
                sb.append(",");
            } while (initpointer != pointer);
        }

        return sb.toString();
    }

    public String getMRU() {
        StringBuilder sb = new StringBuilder();
        if (pointer >= 0) {
            int initpointer = pointer;
            do {
                sb.append(dataSet[pointer]);
                sb.append(",");
                decrement(false);
            } while (initpointer != pointer);
        }
        return sb.toString();
    }

    private void increment(boolean adding) {
        pointer++;
        if (adding) {
            if (pointer >= capacity) {
                pointer = 0;
                capacityReached = true;
            }
        } else {
            if (pointer >= dataSet.length) {
                pointer = 0;
            }
        }
    }

    private void decrement(boolean adding) {
        pointer--;
        if (pointer < 0) {
            if (adding) {
                pointer = capacity - 1;
            } else {
                pointer = dataSet.length - 1;
            }
        }
    }

    public int size() {
        if (capacityReached) {
            return dataSet.length;
        }
        return pointer + 1;
    }
}
