
package com.profiler;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.PopupMenu;
import android.widget.PopupMenu.OnMenuItemClickListener;
import android.widget.TextView;

import com.phoneprofiler.R;
import com.profiler.fragments.ProcessesList;

public class ProcessEditView extends Activity implements OnClickListener, OnMenuItemClickListener {

    private TextView processName;
    private TextView cpuNumber;
    private TextView cpuMaxFrequency;
    private TextView cpuMinFrequency;
    private TextView cpuGovernor;

    private PopupMenu popCpuNumber;
    private PopupMenu popCpuMaxFrequency;
    private PopupMenu popCpuMinFrequency;
    private PopupMenu popCpuGovernor;

    int[] cpuMax;
    int[] cpuMin;
    String[] processGovenors;
    String[] availGovernors;
    String[] availFrequencies;

    CheckBox adjusted;
    CheckBox override;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.activity_process_edit);
        processName = (TextView) findViewById(R.id.process_name);
        cpuNumber = (TextView) findViewById(R.id.process_cpu_value);
        cpuMaxFrequency = (TextView) findViewById(R.id.process_cpu_max_value);
        cpuMinFrequency = (TextView) findViewById(R.id.process_cpu_min_value);
        cpuGovernor = (TextView) findViewById(R.id.process_cpu_governor_value);

        adjusted = (CheckBox) findViewById(R.id.checkbox_process_adjusted);
        override = (CheckBox) findViewById(R.id.checkbox_process_override);
        ((Button) findViewById(R.id.btn_process_confirm)).setOnClickListener(this);
        cpuNumber.setOnClickListener(this);
        cpuMaxFrequency.setOnClickListener(this);
        cpuMinFrequency.setOnClickListener(this);
        cpuGovernor.setOnClickListener(this);

        Bundle info = getIntent().getExtras();
        processName.setText(info.getString(ProcessesList.PROCESS_NAME));
        processGovenors = info.getStringArray(ProcessesList.PROCESS_GOVERNOR);

        cpuNumber.setText("0");
        cpuGovernor.setText(processGovenors[0]);

        availFrequencies = info.getStringArray(ProcessesList.CPU_FREQUENCIES);
        availGovernors = info.getStringArray(ProcessesList.CPU_GOVERNORS);

        generateMenu();

    }

    private void generateMenu() {
        popCpuNumber = new PopupMenu(this, cpuNumber);
        popCpuMaxFrequency = new PopupMenu(this, cpuMaxFrequency);
        popCpuMinFrequency = new PopupMenu(this, cpuMinFrequency);
        popCpuGovernor = new PopupMenu(this, cpuGovernor);
        popCpuNumber.setOnMenuItemClickListener(this);
        popCpuMaxFrequency.setOnMenuItemClickListener(this);
        popCpuMinFrequency.setOnMenuItemClickListener(this);
        popCpuGovernor.setOnMenuItemClickListener(this);

        Menu cpuNumber = popCpuNumber.getMenu();
        Menu cpuMaxFrequency = popCpuMaxFrequency.getMenu();
        Menu cpuMinFrequency = popCpuMinFrequency.getMenu();
        Menu cpuGovernor = popCpuGovernor.getMenu();

        for (int i = 0; i < cpuMax.length; i++) {
            cpuNumber.add(Menu.NONE, i, Menu.NONE, String.valueOf(i));
        }
        for (int i = 0; i < availGovernors.length; i++) {
            cpuGovernor.add(Menu.NONE, i, Menu.NONE, availGovernors[i]);
        }
        for (int i = 0; i < availFrequencies.length; i++) {
            cpuMaxFrequency.add(Menu.NONE, i, Menu.NONE, availFrequencies[i]);
            cpuMinFrequency.add(Menu.NONE, i, Menu.NONE, availFrequencies[i]);
        }
    }

    private static final int CPU_NO = 0;
    private static final int CPU_MAX_FREQ = 1;
    private static final int CPU_MIN_FREQ = 2;
    private static final int GOVERNOR = 3;
    private int selectionMenu = 0;

    @Override
    public boolean onMenuItemClick(MenuItem item) {
        switch (selectionMenu) {
            case CPU_NO:
                cpuNumber.setText(String.valueOf(item.getItemId()));
                break;
            case CPU_MAX_FREQ:
                this.cpuMaxFrequency.setText(availFrequencies[item.getItemId()]);
                break;
            case CPU_MIN_FREQ:
                this.cpuMinFrequency.setText(availFrequencies[item.getItemId()]);
                break;
            case GOVERNOR:
                cpuGovernor.setText(availGovernors[item.getItemId()]);
                break;
        }
        return false;
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.process_cpu_value:
                selectionMenu = CPU_NO;
                popCpuNumber.show();
                break;
            case R.id.process_cpu_max_value:
                selectionMenu = CPU_MAX_FREQ;
                popCpuMaxFrequency.show();
                break;
            case R.id.process_cpu_min_value:
                selectionMenu = CPU_MIN_FREQ;
                popCpuMinFrequency.show();
                break;
            case R.id.process_cpu_governor_value:
                selectionMenu = GOVERNOR;
                popCpuGovernor.show();
                break;
            case R.id.btn_process_confirm:
                Intent result = new Intent();
                Bundle bundle = new Bundle(4);

                result.putExtras(bundle);
                this.setResult(RESULT_OK, result);
                finish();
                break;
        }
    }

}
