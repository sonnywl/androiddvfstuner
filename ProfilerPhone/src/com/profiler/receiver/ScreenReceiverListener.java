package com.profiler.receiver;

public interface ScreenReceiverListener {
    public void onScreenNotify(boolean isScreenOn);
}
