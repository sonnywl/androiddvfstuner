
package com.profiler.receiver;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;

import java.util.ArrayList;

public class ScreenReceiver extends BroadcastReceiver {
    static final String TAG = ScreenReceiver.class.getSimpleName();
    private ArrayList<ScreenReceiverListener> listeners;

    public ScreenReceiver() {
        listeners = new ArrayList<ScreenReceiverListener>();
    }

    @Override
    public void onReceive(Context context, Intent intent) {
        String action = intent.getAction();
        if (action.equals(Intent.ACTION_SCREEN_ON)) {
            notifyListeners(true);
        } else if (action.equals(Intent.ACTION_SCREEN_OFF)) {
            notifyListeners(false);
        }
    }

    public void addScreenListener(ScreenReceiverListener listener) {
        if (listener != null) {
            listeners.add(listener);
        }
    }

    public void removeScreenListener(ScreenReceiverListener listener) {
        if (listener != null) {
            listeners.remove(listener);
        }
    }

    private void notifyListeners(boolean screenOn) {
        for (ScreenReceiverListener listener : listeners) {
            listener.onScreenNotify(screenOn);
        }
    }
    
    public void clearListeners() {
        listeners.clear();
    }
}
