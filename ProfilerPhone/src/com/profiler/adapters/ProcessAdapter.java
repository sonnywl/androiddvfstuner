
package com.profiler.adapters;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import com.phoneprofiler.R;
import com.process.ProcessInfo;

public class ProcessAdapter extends BaseAdapter {
    private LayoutInflater mInflater;
    private ProcessInfo[] processes;

    public ProcessAdapter(Context context, ProcessInfo[] processInfos) {
        mInflater = LayoutInflater.from(context);
        processes = processInfos;
    }

    public void updateAdapterData(ProcessInfo[] info) {
        processes = null;
        processes = info;
        notifyDataSetChanged();
    }

    @Override
    public int getCount() {
        return processes.length;
    }

    @Override
    public Object getItem(int arg0) {
        return processes[arg0];
    }

    @Override
    public long getItemId(int arg0) {
        return arg0;
    }

    @Override
    public View getView(int position, View view, ViewGroup parent) {
        final ViewHolder holder;
        if (view == null) {
            view = mInflater.inflate(R.layout.profiler_adapter_row, parent, false);
            holder = new ViewHolder();
            view.setTag(holder);
        } else {
            holder = (ViewHolder) view.getTag();
        }

        holder.name = (TextView) view.findViewById(R.id.profiler_name);
        holder.description = (TextView) view.findViewById(R.id.profiler_description);
        
        ProcessInfo process = processes[position];
        holder.name.setText(process.getActivityName());
        holder.description.setText(process.toString());

        return view;
    }

    private static class ViewHolder {
        TextView name;
        TextView description;
    }

}
