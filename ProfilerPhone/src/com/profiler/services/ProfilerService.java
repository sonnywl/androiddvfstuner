
package com.profiler.services;

import android.app.Notification;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.app.Service;
import android.content.Intent;
import android.content.IntentFilter;
import android.os.Binder;
import android.os.Bundle;
import android.os.IBinder;

import com.lib.A;
import com.phoneprofiler.R;
import com.process.ProcessInfo;
import com.profiler.ProfilerMain;
import com.profiler.receiver.ScreenReceiver;
import com.profiler.storage.SharedPreferenceManager;

import java.util.Map;

public class ProfilerService extends Service {

    static final String TAG = ProfilerService.class.getSimpleName();

    /** For showing and hiding our notification. */
    private NotificationManager mNM;

    private final IBinder mBinder = new ProfilerBackgroundBinder();

    boolean profilerRun = false;
    boolean analyze = false;
    private CpuProfiler cpuProfiler;
    ScreenReceiver sReceiver;

    @Override
    public void onCreate() {
        A.log(TAG, "Startup Service");

        mNM = (NotificationManager) getSystemService(NOTIFICATION_SERVICE);
        analyze = SharedPreferenceManager.readInfoBoolean(getApplicationContext(), A.ANALYZE);
        cpuProfiler = CpuProfiler.newInstance(this, A.PROFILING_POLLING_INTERVAL);

        IntentFilter intent = new IntentFilter();
        intent.addAction(Intent.ACTION_SCREEN_OFF);
        intent.addAction(Intent.ACTION_SCREEN_ON);

        sReceiver = new ScreenReceiver();
        sReceiver.addScreenListener(cpuProfiler);
        registerReceiver(sReceiver, intent);

        showNotification();
        writeToPreferences();
        cpuProfiler.start();
        profilerRun = true;

    }

    @Override
    public void onTaskRemoved(Intent rootIntent) {
        A.log(TAG, "OnTaskRemoved");
        shutDown();
        super.onTaskRemoved(rootIntent);
    }

    private void writeToPreferences() {
        if (SharedPreferenceManager.readInfoString(getApplicationContext(), A.FREQUENCIES) == null) {
            StringBuilder sb = new StringBuilder(50);
            for (int i = 0; i < cpuProfiler.getAvailableFrequencies().length; i++) {
                sb.append(cpuProfiler.getAvailableFrequencies()[i]);
                if (i != cpuProfiler.getAvailableFrequencies().length - 1) {
                    sb.append(",");
                }
            }
            SharedPreferenceManager.writeInfoString(this, A.FREQUENCIES, sb.toString());
            SharedPreferenceManager.writeInfoStringArray(this, A.GOVERNOR,
                    cpuProfiler.getAvailableGovernors());
        }

    }

    public void clear() {
        cpuProfiler.clear();
    }

    public void shutDown() {
        A.log(TAG, "Shutdown Service");
        sReceiver.clearListeners();
        unregisterReceiver(sReceiver);
        mNM.cancel(R.string.service_running);
        profilerRun = false;
        cpuProfiler.shutdown();
        stopForeground(true);
        stopSelf();
    }

    public Map<String, ProcessInfo> getStatus() {
        return CpuProfiler.processes.getMap();
    }

    /**
     * User level control to stop analyze for cpuThread
     */
    public void updateAnalyze(boolean bool) {
        analyze = bool;
        if (cpuProfiler.isAlive()) {
            cpuProfiler.setAnalyze(analyze);
        }
    }

    public String getCpuStatus() {
        Bundle b = cpuProfiler.getCpuProfilerStatus();
        int[] minCpu = b.getIntArray(CpuProfiler.MAX_FREQ);
        int[] maxCpu = b.getIntArray(CpuProfiler.MIN_FREQ);
        double[] glbCoreUtil = b.getDoubleArray(CpuProfiler.GLB_CORE_UTIL);
        double glbUtil = b.getDouble(CpuProfiler.GLB_UTIL);
        StringBuilder sb = new StringBuilder();
        sb.append(String.format("Global Util:%4.2f%n", glbUtil));
        for (int cpu = 0; cpu < minCpu.length; cpu++) {
            sb.append("util").append(cpu);
            sb.append(String.format(":%10.2f%7s", glbCoreUtil[cpu], " ")).append("\t");
            sb.append("cpu").append(cpu);
            sb.append(String.format(": min %4d %7s max %4d %n", minCpu[cpu], " ", maxCpu[cpu]));
        }
        return sb.toString();
    }

    /**
     * User level control to override a certain process info information
     * 
     * @param processName
     * @param maxFreq
     * @param minFreq
     * @param governor
     */
    public void updateProcess(String processName, int[] maxFreq, int[] minFreq, String[] governor) {
        // ProcessInfo process = infoMap.getMap().get(processName);
        // for (int cpu = 0; cpu < UtilsCpu.getTotalCpus(); cpu++) {
        // process.setGovernor(cpu, governor[cpu]);
        // process.updateCpuMaxFreq(cpu, maxFreq[cpu]);
        // process.updateCpuMinFreq(cpu, minFreq[cpu]);
        // }
    }

    public boolean isProfilerRun() {
        return profilerRun;
    }

    private void showNotification() {
        // In this sample, we'll use the same text for the ticker and the
        // expanded notification
        CharSequence text = getText(R.string.service_running);

        // The PendingIntent to launch our activity if the user selects this
        // notification
        Intent notificationIntent = new Intent(this, ProfilerMain.class);

        notificationIntent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP
                | Intent.FLAG_ACTIVITY_SINGLE_TOP);

        PendingIntent contentIntent = PendingIntent.getActivity(getApplicationContext(), 0,
                notificationIntent, 0);

        // Set the icon, scrolling text and timestamp
        Notification notification = new Notification.Builder(getBaseContext())
                .setContentTitle(text).setContentInfo(getText(R.string.service_label))
                .setSmallIcon(R.drawable.ic_launcher).setContentIntent(contentIntent)
                .setWhen(System.currentTimeMillis()).build();

        notification.flags |= Notification.FLAG_ONGOING_EVENT;

        mNM.notify(R.string.service_running, notification);
    }

    @Override
    public IBinder onBind(Intent intent) {
        return mBinder;
    }

    public class ProfilerBackgroundBinder extends Binder {
        public ProfilerService getService() {
            return ProfilerService.this;
        }
    }

}
