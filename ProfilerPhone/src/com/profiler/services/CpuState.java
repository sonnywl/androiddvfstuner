
package com.profiler.services;

public enum CpuState {
    //@formatter:off 
    HIGH(0.75, "ondemand", 0, 1), 
    MID(0.5, "ondemand", 0, 2), 
    LOW(0.25, "ondemand", 0, 3);
    //@formatter:on
    public final double key;
    public final String governor;
    public int frequencyPosition;
    public int frequencyValue;
    public final int govState;

    CpuState(double key, String governor, int freqPos, int state) {
        this.key = key;
        this.governor = governor;
        this.frequencyPosition = freqPos;
        this.govState = state;
    }

    public void setFreqPos(int freqPos) {
        this.frequencyPosition = freqPos;
    }
}
