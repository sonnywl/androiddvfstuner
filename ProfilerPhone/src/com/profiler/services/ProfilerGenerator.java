
package com.profiler.services;

import android.app.Activity;
import android.app.ActivityManager;
import android.content.Context;

import java.util.HashMap;
import java.util.Map;
import java.util.Random;

public class ProfilerGenerator {

    public static ProfilerGenerator INSTANCE;
    public static final int MAX = 5;
    public static final int MIN = 1;
    private final ActivityManager aManager;
    private static final Random random = new Random();

    public static ProfilerGenerator getInstance(Context context) {
        if (INSTANCE == null) {
            ActivityManager am = (ActivityManager) context
                    .getApplicationContext()
                    .getSystemService(Activity.ACTIVITY_SERVICE);

            INSTANCE = new ProfilerGenerator(am);
        }
        return INSTANCE;
    }

    private ProfilerGenerator(ActivityManager am) {
        aManager = am;
    }

    public long getInputCount() {
        return random.nextInt(500);
    }

    public String getForegroundActivity() {
        return aManager.getRunningTasks(1).get(0).topActivity.flattenToShortString().intern();
    }

    public Map<String, Integer> getProfilerData() {

        HashMap<String, Integer> map = new HashMap<String, Integer>();
        for (int i = 0; i < 10; i++) {
            String key = getForegroundActivity();
            if (map.containsKey(key)) {
                map.put(key, random.nextInt(20));
            } else {
                map.put(key, 1);
            }
        }
        return map;
    }

}
