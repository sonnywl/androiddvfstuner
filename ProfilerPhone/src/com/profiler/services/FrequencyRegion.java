
package com.profiler.services;

public enum FrequencyRegion {
    HIGH, MID, LOW;

    int frequencyPos;

    public void setFrequencyPosition(int pos) {
        this.frequencyPos = pos;
    }
}
