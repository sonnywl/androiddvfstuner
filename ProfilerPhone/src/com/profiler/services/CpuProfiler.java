
package com.profiler.services;

import android.content.Context;
import android.os.Bundle;
import android.os.Handler;
import android.os.Looper;
import android.os.Message;
import android.util.Log;

import com.lib.A;
import com.lib.CircularDoubleArray;
import com.process.ProcessInfo;
import com.process.Shell;
import com.process.UtilsCpu;
import com.profiler.receiver.ScreenReceiverListener;
import com.profiler.storage.ProcessInfoMap;
import com.profiler.storage.SharedPreferenceManager;

import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.RandomAccessFile;

public class CpuProfiler extends Thread implements ScreenReceiverListener {
    private static final String TAG = CpuProfiler.class.getSimpleName();
    public static final String MAX_FREQ = "MaxFreq";
    public static final String MIN_FREQ = "MinFreq";
    public static final String GLB_UTIL = "GlobalUtil";
    public static final String GLB_CORE_UTIL = "GlobalCoreUtil";

    private Context mContext;
    private final int totalCpus;
    private int intervalTime;

    private final UtilsCpu utilsCpu;
    // private final UtilsMmcBlock utilsMmc;
    private final long startTime;

    private long[] prevIdle;
    private long[] prevTotal;

    private ProfilerGenerator profiler;
    /** keep the original state before the profiler started */
    private final int[] formerCpuMax;
    private final int[] formerCpuMin;
    private final String formerGovernor;

    private int[] cpuFreq;
    private int[] currentMaxFreq;
    private int[] currentMinFreq;
    private String currentGovernor;
    // Global averages
    private CircularDoubleArray avgGlobalUtil;
    private CircularDoubleArray avgGlobalCapacity;
    private CircularDoubleArray[] avgGlobalCpuCoreFreq;
    private CircularDoubleArray[] avgGlobalCpuCoreUtil;

    private int[] frequencies;
    private double[] frequencyBarriers;
    private static final int LOWER_FREQ_BARRIER = 0;
    private static final int MID_FREQ_BARRIER = 1;
    private static final int UPPER_FREQ_BARRIER = 2;

    private double[] utilization;

    /** Screen Status */
    public boolean isScreenOn = true;
    /** Used to let the profiler know if the LCD screen is idle or off */
    public boolean isIdle = false;
    /** Let the profiler know if we need to control the CPU */
    public boolean isAnalyze;
    /** Active mapping for what the profiler currently sees */
    public volatile static ProcessInfoMap processes;
    /** stores the current visible activity */
    public ProcessInfo currProcess;

    private Shell shell;

    private ProfilerHandler mHandler;

    private static final int GLOBAL_AVG = 3;

    public static CpuProfiler newInstance(ProfilerService profilerBackgroundService,
            int profilingPollingInterval) {

        Shell shell = Shell.newInstance();
        UtilsCpu utilCpu = UtilsCpu.newInstance(shell, profilerBackgroundService);
        ProfilerGenerator profilerGen = ProfilerGenerator.getInstance(profilerBackgroundService
                .getApplicationContext());
        return new CpuProfiler(profilerBackgroundService, shell, profilerGen, utilCpu,
                profilingPollingInterval);
    }

    private CpuProfiler(ProfilerService service, Shell shellCmd, ProfilerGenerator profilerGen,
            UtilsCpu cpuUtil, int profilingPollingInterval) {
        startTime = System.currentTimeMillis();
        intervalTime = profilingPollingInterval;
        utilsCpu = cpuUtil;
        // utilsMmc = mmcUtil;
        shell = shellCmd;
        profiler = profilerGen;
        totalCpus = UtilsCpu.getTotalCpus();
        frequencies = UtilsCpu.getAvailableFrequencies();

        isAnalyze = service.analyze;
        mContext = service.getApplicationContext();

        cpuFreq = new int[totalCpus];
        currentMaxFreq = new int[totalCpus];
        currentMinFreq = new int[totalCpus];
        currentGovernor = utilsCpu.getCurGovernor(0);

        formerCpuMax = new int[totalCpus];
        formerCpuMin = new int[totalCpus];
        formerGovernor = currentGovernor;

        prevIdle = new long[totalCpus + 1];
        prevTotal = new long[totalCpus + 1];
        utilization = new double[totalCpus + 1];

        avgGlobalUtil = new CircularDoubleArray(GLOBAL_AVG);
        avgGlobalCapacity = new CircularDoubleArray(GLOBAL_AVG);
        avgGlobalCpuCoreFreq = new CircularDoubleArray[totalCpus];
        avgGlobalCpuCoreUtil = new CircularDoubleArray[totalCpus];

        for (int cpu = 0; cpu < totalCpus; cpu++) {
            avgGlobalCpuCoreFreq[cpu] = new CircularDoubleArray(GLOBAL_AVG);
            avgGlobalCpuCoreUtil[cpu] = new CircularDoubleArray(GLOBAL_AVG);
            currentMaxFreq[cpu] = utilsCpu.getMaxFreq(cpu);
            currentMinFreq[cpu] = utilsCpu.getMinFreq(cpu);
            formerCpuMax[cpu] = currentMaxFreq[cpu];
            formerCpuMin[cpu] = currentMinFreq[cpu];
        }

        if (frequencies.length > 1) {
            if (frequencies.length * 0.15 < 1) {
                CpuState.LOW.setFreqPos(1);
            } else {
                CpuState.LOW.setFreqPos((int) Math.floor(frequencies.length * 0.20));
            }
        } else {
            CpuState.LOW.setFreqPos(0);
        }
        CpuState.HIGH.setFreqPos((int) Math.ceil(frequencies.length * 0.7));
        CpuState.MID.setFreqPos((int) Math.floor(frequencies.length * 0.5));
        utilsCpu.switchCpuGovernor("ondemand");
        A.log(TAG, "Len:" + frequencies.length + " LOW:" + CpuState.LOW.frequencyPosition + " MID:"
                + CpuState.MID.frequencyPosition + " HIGH:" + CpuState.HIGH.frequencyPosition);
        frequencyBarriers = new double[3];
        if (frequencies.length > 1) {
            frequencyBarriers[LOWER_FREQ_BARRIER] = (frequencies[0] + frequencies[CpuState.LOW.frequencyPosition]) / 2.5;
            frequencyBarriers[MID_FREQ_BARRIER] = (frequencies[CpuState.LOW.frequencyPosition] + frequencies[CpuState.MID.frequencyPosition]) / 2.5;
            frequencyBarriers[UPPER_FREQ_BARRIER] = (frequencies[frequencies.length - 1] + frequencies[CpuState.HIGH.frequencyPosition]) / 2.5;
        }
        StringBuilder sb = new StringBuilder();
        for (double barrier : frequencyBarriers) {
            sb.append(barrier).append(" ");
        }
        A.log(TAG, sb.toString());
    }

    private ProcessInfo getProcessInfo(String processKey) {
        ProcessInfo process = null;
        if (processes.containsKey(processKey)) {
            process = processes.get(processKey);
        } else {
            process = ProcessInfo.newInstance(processKey, totalCpus);
            for (int cpu = 0; cpu < totalCpus; cpu++) {
                process.setGovernor(cpu, formerGovernor);
            }
            processes.put(processKey, process);
        }
        return process;
    }

    @Override
    public void run() {
        processes = new ProcessInfoMap(SharedPreferenceManager.readHistory(mContext));
        currProcess = getProcessInfo(profiler.getForegroundActivity());
        updateProfiler();
        Looper.prepare();
        mHandler = new ProfilerHandler(Looper.myLooper(), this);
        mHandler.sendEmptyMessageDelayed(LOOP, intervalTime);
        Looper.loop();
    }

    private void updateProfiler() {

        // long time = System.currentTimeMillis();
        double capacity = 0;
        double utilCap = 0;
        getCpuUtilization();
        for (int cpu = 0; cpu < totalCpus; cpu++) {
            utilCap += utilsCpu.getCurFreq(cpu) * utilization[cpu + 1];
        }
        capacity = utilCap
                / (totalCpus * frequencies[UtilsCpu.getAvailableFrequencies().length - 1]);

        String profilerKey = profiler.getForegroundActivity();
        ProcessInfo process = getProcessInfo(profilerKey);

        // If the activity has changed from the currProcess
        if (!currProcess.getActivityName().equals(process.getActivityName())) {
            updateGovernor(process, CpuState.MID);
            currProcess = process;
        }

        // Update the process when we detect there are activities
        if (capacity > 3) {
            if (!isIdle) {
                updateProcess(currProcess, capacity, utilization);
            }
            if (intervalTime > A.PROFILING_POLLING_INTERVAL) {
                intervalTime = A.PROFILING_POLLING_INTERVAL;
            }
            if (isIdle) {
                isIdle = false;
            }
        } else if (isAnalyze) {
            intervalTime = 5000;
            if (!isScreenOn && !isIdle) {
                updateGovernor(process, CpuState.LOW);
            }
            isIdle = true;
        }
        // long totalTime = System.currentTimeMillis() - time;
        // A.log(TAG, "Update " + totalTime);
    }

    private static final int AVG_CAPACITY = 0;
    private static final int STD_DEV = 1;
    private static final int AVG_UTIL = 2;
    private static final int THRESHOLD_STD_DEV = 15;
    private double[] procInfo = new double[3];
    private double[] globalAvgInfo = new double[3];

    /**
     * Updates the process info with the CPU frequency, WiFi, and input state
     * 
     * @param process
     * @param utilization
     */
    private void updateProcess(ProcessInfo process, double globalCapacity, double[] utilization) {
        // Update CPU info
        for (int cpu = 0; cpu < totalCpus; cpu++) {
            cpuFreq[cpu] = utilsCpu.getCurFreq(cpu);
            avgGlobalCpuCoreFreq[cpu].add(cpuFreq[cpu]);
            avgGlobalCpuCoreUtil[cpu].add(utilization[cpu + 1]);
            process.updateCpuFreq(cpu, cpuFreq[cpu]);
            process.updateCpuUtil(cpu, utilization[cpu + 1]);
        }
        // Retrieve Sensor Data;
        avgGlobalCapacity.add(globalCapacity);
        avgGlobalUtil.add(utilization[0]);

        // Retrieve process specific averages
        procInfo[STD_DEV] = process.getStdDev();
        procInfo[AVG_CAPACITY] = process.getAvgCpuCapacity();
        procInfo[AVG_UTIL] = process.getAvgProcessUtil();
        double[] avgCpuFreq = process.getAvgCpuFreq();
        double[] avgCpuUtil = process.getAvgCpuUtil();
        CpuState state = CpuState.LOW;

        if (isAnalyze && process.isReadyForAnalysis()) {
            globalAvgInfo[AVG_CAPACITY] = avgGlobalCapacity.getAverage();
            globalAvgInfo[AVG_UTIL] = avgGlobalUtil.getAverage();
            double globalStdDev = avgGlobalUtil.getStdDev(globalAvgInfo[AVG_UTIL]);

            CpuState[] states = new CpuState[totalCpus];
            for (int i = 0; i < totalCpus; i++) {
                states[i] = CpuState.MID;
            }

            for (int cpu = 0; cpu < avgCpuFreq.length; cpu++) {
                int min = 0;
                int max = frequencies.length - 1;
                // Skips evaluation of core if it isn't utilized
                if (globalAvgInfo[AVG_UTIL] >= procInfo[AVG_UTIL]) {
                    if (avgCpuFreq[cpu] > frequencyBarriers[LOWER_FREQ_BARRIER]
                            || avgGlobalCpuCoreFreq[cpu].getAverage() > frequencyBarriers[LOWER_FREQ_BARRIER] * 0.9) {
                        states[cpu] = CpuState.HIGH;
                        min += CpuState.LOW.frequencyPosition;
                    }
                } else {
                    if (avgCpuFreq[cpu] < frequencyBarriers[MID_FREQ_BARRIER]
                            || avgGlobalCpuCoreFreq[cpu].getAverage() < frequencyBarriers[MID_FREQ_BARRIER] * 0.9) {
                        states[cpu] = CpuState.LOW;
                        max -= CpuState.LOW.frequencyPosition;
                    }
                }
                if (max < min) {
                    max = min;
                }
                updateFrequencies(cpu, min, max, frequencies);
            }
            // Use only the highest CPU State discovered from the evaluation
            for (int cpu = 0; cpu < totalCpus; cpu++) {
                if (states[cpu].ordinal() < state.ordinal()) {
                    state = states[cpu];
                }
            }
            updateGovernor(process, state, procInfo[AVG_UTIL], globalAvgInfo[AVG_UTIL],
                    globalStdDev, true);
        } else {
            for (int cpu = 0; cpu < totalCpus; cpu++) {
                process.updateGovernorState(0);
            }
        }
        if (process.getCountdown() >= 0) {
            process.decrementCountdown();
        }
        // Logging Average for Observations
        process.updateProcessGlobalUtil(utilization[0]);
        process.updateAvgProcessUtil(procInfo[AVG_UTIL]);
        process.updateAvgCpuCapacity(procInfo[AVG_CAPACITY]);

        // Logging for next iterations
        process.updateCpuCapacity(globalCapacity);
        process.updateTime(System.currentTimeMillis() - startTime);

        for (int cpu = 0; cpu < totalCpus; cpu++) {
            process.updateAvgCpuUtil(cpu, avgCpuUtil[cpu]);
            process.updateAvgCpuFreq(cpu, avgCpuFreq[cpu]);
            process.updateCpuMaxFreq(cpu, currentMaxFreq[cpu]);
            process.updateCpuMinFreq(cpu, currentMinFreq[cpu]);
        }
    }

    public void updateFrequencies(int cpu, int min, int max, int[] frequencies) {
        if (frequencies[max] != currentMaxFreq[cpu]) {
            utilsCpu.setMaxFreq(cpu, frequencies[max]);
            currentMaxFreq[cpu] = frequencies[max];
        }
        if (frequencies[min] != currentMinFreq[cpu]) {
            utilsCpu.setMinFreq(cpu, frequencies[min]);
            currentMinFreq[cpu] = frequencies[min];
        }
        A.log(TAG, cpu + " " + currentMinFreq[cpu] + " " + currentMaxFreq[cpu]);

    }

    public void updateGovernor(ProcessInfo process, CpuState state) {
        updateGovernor(process, state, 0, 0, 0, false);
    }

    public void updateGovernor(ProcessInfo process, CpuState state, double procUtil,
            double globalUtil, double globalStdDev, boolean updateState) {

        if (state.governor.equals("ondemand")) {
            if (!utilsCpu.ondemand.isEncountered) {
                utilsCpu.ondemand.setSettings();
            }
            switch (state) {
                case HIGH:
                    variableSamplingRate(procUtil, 90000, 50000, globalUtil);
                    utilsCpu.ondemand.setUpThreshold(75);
                    utilsCpu.ondemand.setPowerSaveBias(0);
                    break;
                default:
                case MID:
                    variableSamplingRate(procUtil, 150000, 90000, globalUtil);
                    utilsCpu.ondemand.setUpThreshold(80);
                    utilsCpu.ondemand.setPowerSaveBias(0);
                    break;
                case LOW:
                    variableSamplingRate(procUtil, 300000, 150000, globalUtil);
                    utilsCpu.ondemand.setUpThreshold(90);
                    screenStateTuning(procUtil, globalStdDev, globalUtil, THRESHOLD_STD_DEV);
                    break;
            }
            if (updateState) {
                process.updateGovernorState(state.govState);
            }
        }
    }

    private void variableSamplingRate(double procUtil, int target, int fallbackTarget,
            double globalUtil) {
        if (procUtil < globalUtil) {
            utilsCpu.ondemand.setSamplingRate(target);
        } else {
            utilsCpu.ondemand.setSamplingRate(fallbackTarget);
        }
    }

    private void screenStateTuning(double procUtil, double globalStdDev, double globalUtil,
            int stdDevThreshold) {
        if (!isScreenOn) {
            utilsCpu.ondemand.setPowerSaveBias(400);
        } else if (procUtil < globalUtil && globalStdDev < stdDevThreshold) {
            utilsCpu.ondemand.setPowerSaveBias(100);
        } else {
            utilsCpu.ondemand.setPowerSaveBias(0);
        }
    }

    private double[] getCpuAverage(CircularDoubleArray[] arr) {
        double[] out = new double[arr.length];
        for (int i = 0; i < arr.length; i++) {
            out[i] = arr[i].getAverage();
        }
        return out;
    }

    @SuppressWarnings("unused")
    private double[] getCpuLoad() {
        double[] cpuLoad = new double[4];
        RandomAccessFile cpuInfo = null;
        try {
            cpuInfo = new RandomAccessFile("/proc/loadavg", "r");
            String[] toks = cpuInfo.readLine().split("\\s+");
            cpuLoad[0] = Double.valueOf(toks[0]);
            cpuLoad[1] = Double.valueOf(toks[1]);
            cpuLoad[2] = Double.valueOf(toks[2]);
            /*
             * The first of these is the number of currently runnable kernel
             * scheduling entities (processes, threads). The value after the
             * slash is the number of kernel scheduling entities that currently
             * exist on the system.
             */
            // String[] threadLoad = toks[3].split("/");
            // cpuLoad[3] = Integer.parseInt(threadLoad[0]) * 1.0 /
            // Integer.parseInt(threadLoad[1]);
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        } finally {
            if (cpuInfo != null) {
                try {
                    cpuInfo.close();
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        }
        return cpuLoad;
    }

    private void getCpuUtilization() {
        RandomAccessFile cpuInfo = null;
        try {
            // monitor total and idle cpu stat of certain process
            cpuInfo = new RandomAccessFile("/proc/stat", "r");
            for (int i = 0; i < totalCpus + 1; i++) {
                String[] toks = cpuInfo.readLine().split("\\s+");

                long idleCpu = Long.parseLong(toks[4]);
                long totalCpu = Long.parseLong(toks[1]) + Long.parseLong(toks[2])
                        + Long.parseLong(toks[3]) + Long.parseLong(toks[5])
                        + Long.parseLong(toks[4]) + Long.parseLong(toks[6])
                        + Long.parseLong(toks[7]);

                long diffIdleCpu = idleCpu - prevIdle[i];
                long diffTotalCpu = totalCpu - prevTotal[i];
                double usage = 1d;
                try {
                    usage = 100 * ((diffTotalCpu - diffIdleCpu) * 1.0 / diffTotalCpu);
                } catch (Exception e) {
                    usage = 100 * ((diffTotalCpu - diffIdleCpu) * 1.0 / 1);
                }
                prevIdle[i] = idleCpu;
                prevTotal[i] = totalCpu;
                utilization[i] = usage;
            }

        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        } finally {
            if (cpuInfo != null) {
                try {
                    cpuInfo.close();
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        }
    }

    public void shutdown() {
        mHandler.sendEmptyMessage(QUIT);
    }

    public int[] getAvailableFrequencies() {
        return UtilsCpu.getAvailableFrequencies();
    }

    public String[] getAvailableGovernors() {
        return UtilsCpu.getAvailableGovernors();
    }

    public int getTotalCpus() {
        return totalCpus;
    }

    private static final int LOOP = 0;
    private static final int QUIT = 1;
    private static final int WAKE = 2;

    class ProfilerHandler extends Handler {
        Looper looper;
        CpuProfiler mProfiler;

        public ProfilerHandler(Looper myLooper, CpuProfiler profiler) {
            looper = myLooper;
            mProfiler = profiler;
        }

        @Override
        public void handleMessage(Message msg) {
            super.handleMessage(msg);
            switch (msg.what) {
                case WAKE:
                    sendEmptyMessageDelayed(LOOP, intervalTime);
                    break;
                case LOOP:
                    mProfiler.updateProfiler();
                    sendEmptyMessageDelayed(LOOP, intervalTime);
                    break;
                case QUIT:
                    looper.quit();
                    setFormer();
                    shell.close();
                    SharedPreferenceManager.writeHistory(mContext, processes.getMap());
                    Log.i(TAG, "Shutdown Complete");
                    break;
            }
        }

    }

    public void setAnalyze(boolean analyze) {
        isAnalyze = analyze;
        if (!isAnalyze) {
            setFormer();
        }
    }

    public void setFormer() {
        for (int cpu = 0; cpu < getTotalCpus(); cpu++) {
            utilsCpu.setMinFreq(cpu, UtilsCpu.getAvailableFrequencies()[0]);
            utilsCpu.setMaxFreq(
                    cpu,
                    UtilsCpu.getAvailableFrequencies()[UtilsCpu.getAvailableFrequencies().length - 1]);
            if (currentGovernor.equals("interactive")) {
                utilsCpu.interactive.reset();
            } else if (currentGovernor.equals("ondemand")) {
                utilsCpu.ondemand.reset();
            }
        }
    }

    @Override
    public void onScreenNotify(boolean screenOn) {
        isScreenOn = screenOn;
    }

    public Bundle getCpuProfilerStatus() {
        Bundle b = new Bundle();
        b.putIntArray(MAX_FREQ, currentMaxFreq);
        b.putIntArray(MIN_FREQ, currentMinFreq);
        b.putDouble(GLB_UTIL, avgGlobalUtil.getAverage());
        b.putDoubleArray(GLB_CORE_UTIL, getCpuAverage(avgGlobalCpuCoreUtil));
        return b;
    }

    public void clear() {
        processes.clear();
    }

}
