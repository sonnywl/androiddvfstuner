
package com.profiler.governors;

public enum Governors {
    CONSERVATIVE("conservative", 100),
    ONDEMAND("ondemand", 50),
    INTERACTIVE("interactive", 25);

    private final String governorName;
    private final int usefulness;

    Governors(String name, int priority) {
        governorName = name;
        usefulness = priority;
    }

    public String getGovernorName() {
        return governorName;
    }

    public int getUsefulness() {
        return usefulness;
    }

}
