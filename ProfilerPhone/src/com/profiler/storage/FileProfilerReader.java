
package com.profiler.storage;

import android.os.Environment;

import com.lib.A;
import com.process.ProcessInfo;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.util.HashMap;
import java.util.Map;

public class FileProfilerReader {
    private static final String TAG = "FileProfilerReader";
    private static final String ABSOLUTE_DIR = Environment
            .getExternalStorageDirectory()
            .getAbsoluteFile() + File.separator + "data" + File.separator + "ProfilerData.csv";

    public static Map<String, ProcessInfo> read() {
        Map<String, ProcessInfo> map = new HashMap<String, ProcessInfo>();
        File file = new File(ABSOLUTE_DIR);
        BufferedReader br = null;
        if (file.exists()) {
            try {
                br = new BufferedReader(new FileReader(ABSOLUTE_DIR));
                String process;
                while ((process = br.readLine()) != null) {
                    String[] basic = process.split(",");
                    for (int i = 0; i < basic.length; i++) {

                    }
                }
            } catch (FileNotFoundException e) {
                e.printStackTrace();
            } catch (IOException e) {
                e.printStackTrace();
            } finally {
                try {
                    if (br != null)
                        br.close();
                } catch (IOException ex) {
                    ex.printStackTrace();
                }
            }
            return map;
        }
        A.log(TAG, "File Does not Exists " + ABSOLUTE_DIR);
        return null;
    }
}
