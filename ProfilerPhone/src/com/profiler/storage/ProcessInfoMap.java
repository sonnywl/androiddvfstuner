
package com.profiler.storage;

import com.process.ProcessInfo;

import java.util.LinkedList;
import java.util.Map;

/**
 * Mapping of process info and a queue that allows the cpu manager to determine
 * which processes to consider evaluating
 * 
 * @author sonny
 */
public class ProcessInfoMap {
    Map<String, ProcessInfo> cpuStatus;
    private LinkedList<String> queue;

    public ProcessInfoMap(Map<String, ProcessInfo> map) {
        queue = new LinkedList<String>();
        cpuStatus = map;
    }

    public void populateQueue() {
        for (ProcessInfo process : cpuStatus.values()) {
            if (!process.isAdjusted()) {
                queueProcessInfoCheck(process.getActivityName());
            }
        }
    }

    /**
     * Submits process info for the algorithm to check
     */
    public void queueProcessInfoCheck(String processName) {
        queue.add(processName);
    }

    public boolean isQueueEmpty() {
        return queue.isEmpty();
    }

    public String checkNextProcess() {
        return queue.pop();
    }

    public Map<String, ProcessInfo> getMap() {
        return cpuStatus;
    }

    public void clear() {
        queue.clear();
        cpuStatus.clear();
    }
    // ----- Abstractions
    public boolean containsKey(String key) {
        return cpuStatus.containsKey(key);
    }

    public ProcessInfo get(String key) {
        return cpuStatus.get(key);
    }

    public void put(String key, ProcessInfo process) {
        cpuStatus.put(key, process);
    }
}
