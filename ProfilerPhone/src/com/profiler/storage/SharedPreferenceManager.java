
package com.profiler.storage;

import android.app.Activity;
import android.content.Context;
import android.content.SharedPreferences;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.reflect.TypeToken;
import com.lib.A;
import com.process.ProcessInfo;

import java.lang.reflect.Type;
import java.util.Arrays;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;

public class SharedPreferenceManager {
    public static final String TAG = SharedPreferenceManager.class.getSimpleName();
    public static Map<String, ProcessInfo> map;
    public static Set<String> frequencies;
    public static Set<String> governors;

    public static void writeInfoBoolean(final Context mContext, final String key, final boolean bool) {
        new Thread(new Runnable() {
            @Override
            public void run() {
                SharedPreferences mPrefs = mContext.getSharedPreferences(A.PREFERENCE,
                        Activity.MODE_PRIVATE);
                SharedPreferences.Editor prefsEditor = mPrefs.edit();
                prefsEditor.putBoolean(key, bool);
                prefsEditor.commit();
            }

        }).start();
    }

    public static void writeInfoString(final Context mContext, final String key, final String msg) {
        new Thread(new Runnable() {
            @Override
            public void run() {
                SharedPreferences mPrefs = mContext.getSharedPreferences(A.PREFERENCE,
                        Activity.MODE_PRIVATE);
                SharedPreferences.Editor prefsEditor = mPrefs.edit();
                prefsEditor.putString(key, msg);
                prefsEditor.commit();
            }

        }).start();
    }

    public static void writeInfoStringArray(final Context mContext, final String key,
            final String[] msg) {
        new Thread(new Runnable() {
            @Override
            public void run() {
                SharedPreferences mPrefs = mContext.getSharedPreferences(A.PREFERENCE,
                        Activity.MODE_PRIVATE);
                SharedPreferences.Editor prefsEditor = mPrefs.edit();
                prefsEditor.putStringSet(key, new HashSet<String>(Arrays.asList(msg)));
                prefsEditor.commit();
            }

        }).start();
    }

    public static boolean readInfoBoolean(final Context mContext, final String key) {
        SharedPreferences mPrefs = mContext.getSharedPreferences(A.PREFERENCE,
                Activity.MODE_PRIVATE);
        return mPrefs.getBoolean(key, false);
    }

    public static String readInfoString(final Context mContext, final String key) {
        SharedPreferences mPrefs = mContext.getSharedPreferences(A.PREFERENCE,
                Activity.MODE_PRIVATE);
        return mPrefs.getString(key, null);
    }

    public static Set<String> readInfoStringSet(final Context mContext, final String key) {
        SharedPreferences mPrefs = mContext.getSharedPreferences(A.PREFERENCE,
                Activity.MODE_PRIVATE);
        return mPrefs.getStringSet(key, null);
    }

    public static void writeHistory(final Context mContext, final Map<String, ProcessInfo> infoMap) {
        new Thread(new Runnable() {
            @Override
            public void run() {
                SharedPreferences mPrefs = mContext.getSharedPreferences(A.PREFERENCE,
                        Activity.MODE_PRIVATE);
                SharedPreferences.Editor prefsEditor = mPrefs.edit();
                Gson gson = new GsonBuilder().enableComplexMapKeySerialization().create();
                String json = gson.toJson(infoMap);
                prefsEditor.putString("MyObject", json);
                prefsEditor.commit();
            }

        }).start();
    }

    /**
     * Recover the last known instance
     */
    public static Map<String, ProcessInfo> readHistory(final Context mContext) {
        if (map != null) {
            return map;
        }
        SharedPreferences mPrefs = mContext.getSharedPreferences(A.PREFERENCE,
                Activity.MODE_PRIVATE);
        Type typeOfHashMap = new TypeToken<Map<String, ProcessInfo>>() {
        }.getType();
        Gson gson = new GsonBuilder().enableComplexMapKeySerialization().create();
        String json = mPrefs.getString("MyObject", "");
        try {
            map = gson.fromJson(json, typeOfHashMap);
            if (map != null)
                return map;
        } catch (Exception e) {
            // Someone updated ProcessInfo lol, reseting the data
        }

        map = new HashMap<String, ProcessInfo>(10);
        return map;
    }

    public static void clear(final Context mContext) {
        SharedPreferences mPrefs = mContext.getSharedPreferences(A.PREFERENCE,
                Activity.MODE_PRIVATE);
        SharedPreferences.Editor prefsEditor = mPrefs.edit();
        prefsEditor.clear();
        prefsEditor.commit();
    }
}
