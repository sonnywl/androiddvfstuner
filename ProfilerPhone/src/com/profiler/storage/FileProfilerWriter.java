
package com.profiler.storage;

import android.os.Environment;
import android.util.Log;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.Calendar;
import java.util.Queue;
import java.util.concurrent.ArrayBlockingQueue;

public class FileProfilerWriter implements Runnable {
    private static final String TAG = "FileWriterQueue";
    private Queue<String> outputContent;
    private final String RELATIVE_DIR;
    private final String ABSOLUTE_DIR;
    private final String fileName;
    volatile boolean writeToFile;
    private boolean append = false;
    private static final int year = Calendar.getInstance().get(Calendar.YEAR);
    private static final int month = Calendar.getInstance().get(Calendar.MONTH) + 1;
    private static final int day = Calendar.getInstance().get(Calendar.DAY_OF_MONTH);

    public FileProfilerWriter() {
        this(500, null, false);
    }

    public FileProfilerWriter(int capacity) {
        this(capacity, null, false);
    }

    public FileProfilerWriter(int capacity, final boolean append) {
        this(capacity, null, append);
    }
    
    public FileProfilerWriter(final int capacity, final String filename, final boolean append) {
        if (filename != null) {
            fileName = filename;
        } else {
            fileName = year + "-" + month + "-" + day + "-" + "ProfilerData.csv";
        }
        this.append = append;
        outputContent = new ArrayBlockingQueue<String>(capacity);
        File externalDir = Environment.getExternalStorageDirectory().getAbsoluteFile();
        RELATIVE_DIR = externalDir + File.separator + "data";
        ABSOLUTE_DIR = RELATIVE_DIR + File.separator + fileName;
    }

    public void add(String data) {
        outputContent.add(data);
    }

    @Override
    public void run() {
        try {
            if (Environment.getExternalStorageState().equals(Environment.MEDIA_MOUNTED)) {
                Log.i(TAG, "File at " + ABSOLUTE_DIR);
                File fileFolder = new File(RELATIVE_DIR);

                File file = new File(ABSOLUTE_DIR);
                FileOutputStream fow = null;
                if (!fileFolder.exists()) {
                    fileFolder.mkdir();
                }
                if (!file.exists()) {
                    file.createNewFile();
                }
                fow = new FileOutputStream(file, append);

                while (outputContent.size() > 0) {
                    String output = outputContent.remove().toString();
                    fow.write(output.getBytes());
                    fow.flush();
                }

                if (fow != null) {
                    fow.close();
                    fow = null;
                }
                Log.i(TAG, "FileWriterQueue Closing");
            }
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}
