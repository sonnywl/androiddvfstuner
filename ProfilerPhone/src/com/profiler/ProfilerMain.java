
package com.profiler;

import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.content.ServiceConnection;
import android.os.Bundle;
import android.os.IBinder;
import android.support.v4.app.FragmentActivity;
import android.util.Log;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.Toast;

import com.lib.A;
import com.phoneprofiler.R;
import com.process.ProcessInfo;
import com.profiler.fragments.InputProfiler;
import com.profiler.fragments.ProcessesList;
import com.profiler.services.ProfilerService;
import com.profiler.services.ProfilerService.ProfilerBackgroundBinder;
import com.profiler.storage.FileProfilerWriter;
import com.profiler.storage.SharedPreferenceManager;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Map;

public class ProfilerMain extends FragmentActivity implements
        ServiceConnection {

    private static final String TAG = "ProfilerPhoneMain";
    /** Is the activity bound to Service */
    private boolean mBound = false;
    private boolean isAnalyze, statsState = false;
    private ProfilerService mService;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        isAnalyze = SharedPreferenceManager.readInfoBoolean(getApplicationContext(), A.ANALYZE);
        setContentView(R.layout.activity_profiler_main);
        new Thread(new Runnable() {
            @Override
            public void run() {
                SharedPreferenceManager.readHistory(getApplicationContext());
                try {
                    Runtime.getRuntime().exec(A.getSudo());
                } catch (IOException e) {
                    e.printStackTrace();
                }

            }
        }).start();

        if (savedInstanceState == null) {
            getSupportFragmentManager().beginTransaction()
                    .add(R.id.main_container, new InputProfiler())
                    .commit();
        }

    }

    public void updateAnalyze(boolean bool) {
        if (mService != null && mService.isProfilerRun()) {
            mService.updateAnalyze(bool);
        }
        isAnalyze = bool;
    }

    public void updateServiceData(String processName, int[] maxFreq, int[] minFreq,
            String[] governor) {
        if (mService != null) {
            mService.updateProcess(processName, maxFreq, minFreq, governor);
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.profiler_phone_main, menu);
        for (int i = 0; i < menu.size(); i++) {
            if (menu.getItem(i).getItemId() == R.id.action_statistics) {
                if (statsState) {
                    menu.getItem(i).setTitle(R.string.home);
                } else {
                    menu.getItem(i).setTitle(R.string.statistics);
                }
            }
        }

        return true;
    }

    public void mainOnClick(View view) {
        Intent intent = new Intent(this, ProfilerService.class);
        switch (view.getId()) {
            case R.id.btn_start:
                if (!mBound) {
                    getApplicationContext().bindService(intent, this, Context.BIND_AUTO_CREATE);
                } else {
                    Log.i(TAG, "Already Bound");
                }
                break;
            case R.id.btn_stop:
                unBind();
                break;
            case R.id.btn_refresh:
                if (mBound) {
                    Log.i(TAG, mService.getCpuStatus());
                }
                break;
            case R.id.btn_processes_statistics:
                ProcessesList list = (ProcessesList) getSupportFragmentManager().findFragmentById(
                        R.id.main_container);
                list.updateDataSet(getProcessRecords());
                break;
        }
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.action_statistics:
                if (statsState) {
                    getSupportFragmentManager().beginTransaction()
                            .replace(R.id.main_container, new InputProfiler())
                            .commit();
                } else {
                    getSupportFragmentManager().beginTransaction()
                            .replace(R.id.main_container, new ProcessesList())
                            .commit();
                }
                statsState = statsState ? false : true;
                invalidateOptionsMenu();
                break;
            case R.id.action_clear:
                SharedPreferenceManager.clear(getApplicationContext());
                if (mService != null) {
                    mService.clear();
                }
                break;
            case R.id.menu_save:
                FileProfilerWriter fpw = new FileProfilerWriter();
                if (mService != null) {
                    Collection<ProcessInfo> collection = mService.getStatus().values();
                    StringBuilder sb = new StringBuilder(collection.size() * 10);
                    for (ProcessInfo info : collection) {
                        sb.append(info.flushData());
                    }
                    fpw.add(sb.toString());
                } else {
                    Collection<ProcessInfo> collection = SharedPreferenceManager.readHistory(
                            getApplicationContext()).values();
                    StringBuilder sb = new StringBuilder(collection.size() * 10);
                    for (ProcessInfo info : collection) {
                        sb.append(info.flushData());
                    }
                    fpw.add(sb.toString());
                    Toast.makeText(getApplicationContext(), "Saving Shared Preferences Version",
                            Toast.LENGTH_SHORT).show();
                }
                new Thread(fpw).start();
            default:
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onServiceConnected(ComponentName name, IBinder service) {
        ProfilerBackgroundBinder binder = (ProfilerBackgroundBinder) service;
        mService = binder.getService();
        mService.updateAnalyze(isAnalyze);
        mBound = true;
        Log.i(TAG, "Service is connected " + mService.hashCode());
    }

    @Override
    public void onServiceDisconnected(ComponentName name) {
        Log.i(TAG, "Service is disconnected" + mService.hashCode());
    }

    private void unBind() {
        if (mBound) {
            getApplicationContext().unbindService(this);
            mService.shutDown();
            mBound = false;
        } else {
            Log.i(TAG, "service is already unbounded");
        }
    }

    @Override
    protected void onStop() {
        SharedPreferenceManager.writeInfoBoolean(getApplicationContext(), A.ANALYZE, isAnalyze);
        if (mService != null) {
            SharedPreferenceManager.writeHistory(this, mService.getStatus());
        }
        super.onStop();
    }

    public ArrayList<ProcessInfo> getProcessRecords() {
        if (mService != null) {
            return new ArrayList<ProcessInfo>(mService.getStatus().values());
        }

        Map<String, ProcessInfo> info = SharedPreferenceManager
                .readHistory(getApplicationContext());
        if (info != null) {
            return new ArrayList<ProcessInfo>(info.values());
        }
        return null;
    }

}
