
package com.profiler.fragments;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.ListFragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ListView;

import com.lib.A;
import com.phoneprofiler.R;
import com.process.ProcessInfo;
import com.profiler.ProfilerMain;
import com.profiler.adapters.ProcessAdapter;

import java.util.ArrayList;
import java.util.Collection;

public class ProcessesList extends ListFragment {
    public static final String TAG = "ProcessesList";
    public static final int PROCESS_EDIT = 0;
    public static final int KEY = 2;
    public static final String PROCESS_NAME = "ProcessName";
    public static final String PROCESS_CPU_MAX = "ProcessCpuMax";
    public static final String PROCESS_CPU_MIN = "ProcessCpuMin";
    public static final String PROCESS_INPUT = "ProcessInput";
    public static final String PROCESS_GOVERNOR = "ProcessGovernor";
    public static final String CPU_FREQUENCIES = "Frequencies";
    public static final String CPU_GOVERNORS = "Governors";
    private ProcessAdapter adapter;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        return inflater.inflate(R.layout.fragment_processes, container, false);
    }

    @Override
    public void onActivityCreated(Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        if (savedInstanceState == null) {
            ArrayList<ProcessInfo> info = ((ProfilerMain) getActivity()).getProcessRecords();
            adapter = new ProcessAdapter(getActivity().getBaseContext(),
                    info.toArray(new ProcessInfo[info.size()]));
            setListAdapter(adapter);
        }
        registerForContextMenu(getListView());
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        if (data != null && resultCode == Activity.RESULT_OK) {
            switch (requestCode) {
                case PROCESS_EDIT:
                    A.log(TAG, "Results from edit");
                    Bundle bundle = data.getBundleExtra(TAG);
                    ((ProfilerMain) getActivity()).updateServiceData(
                            bundle.getString(PROCESS_NAME), bundle.getIntArray(PROCESS_CPU_MAX),
                            bundle.getIntArray(PROCESS_CPU_MIN),
                            bundle.getStringArray(PROCESS_GOVERNOR));
                    break;
                default:
            }
        }
    }

    @Override
    public void onListItemClick(ListView l, View v, int position, long id) {
        super.onListItemClick(l, v, position, id);
        Log.i(TAG, adapter.getItem(position).toString());
//        ProcessInfo info = ((ProcessInfo) adapter.getItem(position));
//        Intent intent = new Intent(getActivity().getApplicationContext(), ProcessEditView.class);
//        Bundle bundle = new Bundle(6);
//        bundle.putString(PROCESS_NAME, info.getActivityName());
//        bundle.putStringArray(PROCESS_GOVERNOR, info.getGovernors());
//        intent.putExtras(bundle);
//        startActivityForResult(intent, PROCESS_EDIT);
    }

    @Override
    public void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
    }

    public void updateDataSet(final Collection<ProcessInfo> collection) {
        if (collection != null) {
            adapter.updateAdapterData(collection.toArray(new ProcessInfo[collection.size()]));
        }
    }
}
