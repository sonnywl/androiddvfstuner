
package com.profiler.fragments;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.CompoundButton.OnCheckedChangeListener;
import android.widget.RelativeLayout;

import com.lib.A;
import com.phoneprofiler.R;
import com.profiler.ProfilerMain;
import com.profiler.storage.SharedPreferenceManager;

public class InputProfiler extends Fragment implements OnCheckedChangeListener {

    private CheckBox enableCpuManager;
    private static final String TAG = InputProfiler.class.getSimpleName();

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        RelativeLayout rl = (RelativeLayout) inflater.inflate(R.layout.fragment_input, container,
                false);
        enableCpuManager = (CheckBox) rl.findViewById(R.id.profiler_main_checkbox);
        if (savedInstanceState != null) {
            enableCpuManager.setChecked(savedInstanceState.getBoolean(TAG));
        } else {
            enableCpuManager.setChecked(SharedPreferenceManager.readInfoBoolean(getActivity()
                    .getApplicationContext(), A.ANALYZE));
        }
        enableCpuManager.setOnCheckedChangeListener(this);
        return rl;
    }

    @Override
    public void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
        outState.putBoolean(TAG, enableCpuManager.isChecked());
    }

    @Override
    public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
        ((ProfilerMain) getActivity()).updateAnalyze(isChecked);
        enableCpuManager.setChecked(isChecked);
    }

}
