
package com.process;

import com.lib.A;
import com.lib.CircularDoubleArray;
import com.lib.CircularIntArray;
import com.lib.CircularLongArray;

/**
 * ProcessInfo retains the information about a specific process information
 * about their cpu frequency and input size It also retains wifi info, but that
 * isn't specific to this process
 * 
 * @author sonny
 */
public class ProcessInfo {
    static final String TAG = "ProcessInfo";

    public static ProcessInfo newInstance(String activityTask, int totalCpus) {

        return new ProcessInfo(activityTask, totalCpus);
    }

    /**
     * cpuStats stores the frequency of frequencies provided to identify the
     * common frequencies this process uses
     */
    private final String activityName;
    private int countdown = 5;
    private CircularIntArray[] cpuMaxFreq;
    private CircularIntArray[] cpuMinFreq;
    private String[] currGovernor;
    private CircularLongArray time;

    private CircularIntArray inputCount;
    private CircularDoubleArray cpuGlobalCapacity;
    private CircularDoubleArray cpuGlobalAvgCapacity;
    private CircularDoubleArray[] cpuAvgFreq;
    private CircularDoubleArray cpuGlobalUtil;
    private CircularDoubleArray cpuGlobalAvgUtil;
    private CircularDoubleArray[] cpuCoreUtil;
    private CircularDoubleArray[] cpuCoreAvgUtil;
    private CircularIntArray[] cpuFreq;
    private CircularIntArray cpuGovernorState;

    private boolean adjusted = false;
    private boolean analyze = false;

    private ProcessInfo(String activityTask, int totalCpus) {
        activityName = activityTask;
        currGovernor = new String[totalCpus];
        cpuMaxFreq = new CircularIntArray[totalCpus];
        cpuMinFreq = new CircularIntArray[totalCpus];
        cpuAvgFreq = new CircularDoubleArray[totalCpus];

        cpuFreq = new CircularIntArray[totalCpus];
        cpuCoreUtil = new CircularDoubleArray[totalCpus];
        cpuCoreAvgUtil = new CircularDoubleArray[totalCpus];

        cpuGovernorState = new CircularIntArray(A.MEMORY_STORAGE);
        cpuGlobalAvgCapacity = new CircularDoubleArray(A.MEMORY_STORAGE);
        cpuGlobalCapacity = new CircularDoubleArray(A.MEMORY_STORAGE);

        cpuGlobalUtil = new CircularDoubleArray(A.MEMORY_STORAGE);
        cpuGlobalAvgUtil = new CircularDoubleArray(A.MEMORY_STORAGE);
        
        inputCount = new CircularIntArray(A.MEMORY_STORAGE);
        time = new CircularLongArray(A.MEMORY_STORAGE);

        for (int cpu = 0; cpu < totalCpus; cpu++) {
            cpuFreq[cpu] = new CircularIntArray(A.MEMORY_STORAGE);
            cpuAvgFreq[cpu] = new CircularDoubleArray(A.MEMORY_STORAGE);
            cpuMaxFreq[cpu] = new CircularIntArray(A.MEMORY_STORAGE);
            cpuMinFreq[cpu] = new CircularIntArray(A.MEMORY_STORAGE);
            cpuCoreUtil[cpu] = new CircularDoubleArray(A.MEMORY_STORAGE);
            cpuCoreAvgUtil[cpu] = new CircularDoubleArray(A.MEMORY_STORAGE);
        }
    }

    public void clearData() {
        for (int cpu = 0; cpu < cpuAvgFreq.length; cpu++) {
            cpuAvgFreq[cpu].clear();
        }
        cpuGlobalAvgCapacity.clear();
        inputCount.clear();
    }

    /**
     * Decrement Countdown, once it hits -1 then we allow this process to be
     * analyzed by the CpuAlgorithm
     */
    public void decrementCountdown() {
        if (countdown >= 0) {
            countdown--;
        }
    }

    public String getActivityName() {
        return activityName;
    }

    public double getAvgCpuCapacity() {
        return cpuGlobalCapacity.getAverage();
    }

    public double[] getAvgCpuUtil() {
        double[] cpuAvg = new double[cpuAvgFreq.length];
        for (int cpu = 0; cpu < cpuFreq.length; cpu++) {
            cpuAvg[cpu] = cpuCoreUtil[cpu].getAverage();
        }
        return cpuAvg;
    }

    public double[] getAvgCpuFreq() {
        double[] cpuAvg = new double[cpuAvgFreq.length];
        for (int cpu = 0; cpu < cpuFreq.length; cpu++) {
            cpuAvg[cpu] = cpuFreq[cpu].getAverage();
        }
        return cpuAvg;
    }

    public double getStdDev() {
        return cpuGlobalUtil.getStdDev();
    }
    public double getAvgProcessUtil() {
        return cpuGlobalUtil.getAverage();
    }

    public double getAvgInputCount() {
        return inputCount.getAverage();
    }

    public String getCurrGovernor(int cpu) {
        return currGovernor[cpu];
    }

    public int getCountdown() {
        return countdown;
    }

    public String[] getGovernors() {
        return currGovernor;
    }

    public boolean isAdjusted() {
        return adjusted;
    }

    public boolean isAnalyze() {
        return analyze;
    }

    public boolean isReadyForAnalysis() {
        return this.countdown <= 0;
    }

    public void setAdjusted(boolean adjusted) {
        this.adjusted = adjusted;
    }

    public void setAnalyze(boolean analyze) {
        this.analyze = analyze;
    }

    public void setGovernor(int cpu, String currGovernor) {
        this.currGovernor[cpu] = currGovernor;
    }

    public void updateAvgCpuCapacity(double capacity) {
        cpuGlobalAvgCapacity.add(capacity);
    }

    public void updateAvgProcessUtil(double util) {
        cpuGlobalAvgUtil.add(util);
    }

    public void updateAvgCpuFreq(int cpu, double freq) {
        cpuAvgFreq[cpu].add(freq);
    }

    public void updateAvgCpuUtil(int cpu, double utilAvg) {
        cpuCoreAvgUtil[cpu].add(utilAvg);
    }

    public void updateCpuCapacity(double capacity) {
        cpuGlobalCapacity.add(capacity);
    }

    public void updateCpuFreq(int cpu, int freq) {
        cpuFreq[cpu].add(freq);
    }

    public void updateCpuUtil(int cpu, double util) {
        cpuCoreUtil[cpu].add(util);
    }

    public void updateProcessGlobalUtil(double utilization) {
        cpuGlobalUtil.add(utilization);
    }

    public void updateCpuMaxFreq(int cpu, int cpuFreq) {
        cpuMaxFreq[cpu].add(cpuFreq);
    }

    public void updateCpuMinFreq(int cpu, int cpuFreq) {
        cpuMinFreq[cpu].add(cpuFreq);
    }

    public void updateGovernorState(int state) {
        cpuGovernorState.add(state);
    }

    public void updateInputEventCount(int touchEventCount) {
        inputCount.add(touchEventCount);
    }

    public void updateTime(long timeStamp) {
        time.add(timeStamp);
    }

    public String flushData() {
        StringBuilder sb = new StringBuilder(A.MEMORY_STORAGE * 6);
        sb.append(this.getActivityName()).append("\n");
        outputData(sb, "time", time);
        outputData(sb, "util", cpuGlobalUtil);
        outputData(sb, "utilAvg", cpuGlobalAvgUtil);

        for (int cpu = 0; cpu < cpuAvgFreq.length; cpu++) {
            outputData(sb, "util" + cpu, cpuCoreUtil[cpu]);
            outputData(sb, "utilAvg" + cpu, cpuCoreAvgUtil[cpu]);
        }

        for (int cpu = 0; cpu < cpuAvgFreq.length; cpu++) {
            outputData(sb, "cur" + cpu, cpuFreq[cpu]);
            outputData(sb, "cpuAvg" + cpu, cpuAvgFreq[cpu]);
        }

        outputData(sb, "gov", cpuGovernorState);

        for (int cpu = 0; cpu < cpuAvgFreq.length; cpu++) {
            outputData(sb, "max" + cpu, cpuMaxFreq[cpu]);
            outputData(sb, "min" + cpu, cpuMinFreq[cpu]);
        }
        return sb.toString();
    }

    public String toString() {
        StringBuilder sb = new StringBuilder();
        double[] util = this.getAvgCpuUtil();
        for (int cpu = 0; cpu < cpuFreq.length; cpu++) {
            sb.append("util").append(cpu);
            sb.append(String.format("%10.2f%7s", util[cpu], " ")).append("\t");
            sb.append("cpu").append(cpu);
            sb.append(String.format("%15.2f%7s%n", cpuAvgFreq[cpu].getAverage(), " "));
        }
        return sb.toString();
    }

    private void outputData(StringBuilder sb, String name, CircularDoubleArray arr) {
        sb.append(name).append(",").append(arr.getMRU()).append("\n");
    }

    private void outputData(StringBuilder sb, String name, CircularIntArray arr) {
        sb.append(name).append(",").append(arr.getMRU()).append("\n");
    }

    private void outputData(StringBuilder sb, String name, CircularLongArray arr) {
        sb.append(name).append(",").append(arr.getMRU()).append("\n");
    }
}
