

package com.process;

import com.lib.A;

import java.io.BufferedOutputStream;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.RandomAccessFile;

public class Shell {
    private static Shell INSTANCE;
    static final String TAG = Shell.class.getSimpleName();
    private StringBuilder sb = new StringBuilder();
    private Process process;
    private BufferedOutputStream bos;
    private BufferedReader br;

    public static Shell newInstance() {
        if (INSTANCE == null) {
            INSTANCE = new Shell();
        }
        return INSTANCE;
    }

    public Shell() {
        process = null;
        try {
            process = Runtime.getRuntime().exec(A.getSudo());
            bos = new BufferedOutputStream(process.getOutputStream());
            br = new BufferedReader(new InputStreamReader(process.getInputStream()), 1024);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public static void changeMod(StringBuilder sb, String fileDes) {
        sb.append("chmod 666 ").append(fileDes).append("\n");
    }

    public static void changeOwner(StringBuilder sb, long uid, String fileDes) {
        sb.append("chown ").append("system").append(":").append("system").append(" ")
                .append(fileDes).append("\n");
    }

    public String readCommand(String command, boolean multipleLines) {
        RandomAccessFile raf = null;
        try {
            raf = new RandomAccessFile(command, "r");
            String line = null;
            while ((line = raf.readLine()) != null) {
                sb.append(line);
                if (multipleLines) {
                    sb.append("\n");
                }
            }
            raf.close();
        } catch (IOException e) {
            A.log(TAG, "Exception File " + command);
        } finally {
            try {
                if (raf != null) {
                    raf.close();
                }
            } catch (IOException e) {
                e.printStackTrace();
            }

        }
        String tmp = sb.toString().intern();
        if (tmp.length() == 0) {
            tmp = "0".intern();
        }
        sb.setLength(0);
        return tmp;
    }

    public void writeCommand(int value, String file) {
        writeCommand(String.valueOf(value), file);
    }

    public void writeCommand(String value, String file) {
        try {
            RandomAccessFile raf = new RandomAccessFile(file, "rw");
            raf.write(value.getBytes());
            raf.close();
        } catch (Exception e) {
            A.log(TAG, "Exception File " + file);
            writeCommandSysFs("echo " + value + " > " + file);
        }
    }

    public void writeCommandSysFs(String command) {
        try {
            A.log(TAG, command);
            bos.write((command + "\n").getBytes());
            bos.flush();
        } catch (IOException e) {
            A.log(TAG, "Write to Sysfs failed:" + command);
        }
    }

    public void close() {
        try {
            bos.close();
            br.close();
            process.destroy();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}
