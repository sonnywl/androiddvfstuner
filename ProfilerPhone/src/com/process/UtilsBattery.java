
package com.process;

public class UtilsBattery {
    private static final String BATTERY = "/sys/class/power_supply/battery/";
    private static final String CAPACITY = BATTERY + "capacity";
    private static final String VOLTAGE_NOW = BATTERY + "voltage_now";

    private Shell shell;

    public UtilsBattery(Shell shell) {
        this.shell = shell;
    }

    public String getCapacity() {
        return shell.readCommand(CAPACITY, false);
    }

    public String getVoltageNow() {
        return shell.readCommand(VOLTAGE_NOW, false);
    }
}
