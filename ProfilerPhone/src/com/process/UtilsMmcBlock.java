
package com.process;

public class UtilsMmcBlock {
    static final String TAG = UtilsMmcBlock.class.getCanonicalName();

    private static final String MMC_BLK = "/sys/block/mmcblk0/";
    private static final String MMC_BLK_STAT = MMC_BLK + "stat";
    private static final String MMC_BLK_SCHED = MMC_BLK + "queue/scheduler";

    private final Shell shell;
    private static UtilsMmcBlock INSTANCE;

    public static final int READ_IO = 0;
    public static final int READ_MERGES = 1;
    public static final int READ_SECTORS = 2;
    public static final int READ_TICKS = 3;
    public static final int WRITE_IO = 4;
    public static final int WRITE_MERGES = 5;
    public static final int WRITE_SECTORS = 6;
    public static final int WRITE_TICKS = 7;
    public static final int IN_FLIGHT = 8;
    public static final int IO_TICKS = 9;
    public static final int TIME_IN_QUEUE = 10;

    public static UtilsMmcBlock newInstance(Shell shell) {
        if (INSTANCE != null) {
            return INSTANCE;
        }
        INSTANCE = new UtilsMmcBlock(shell);
        return INSTANCE;
    }

    private UtilsMmcBlock(Shell shell) {
        this.shell = shell;
    }

    public String[] readStat() {
        return shell.readCommand(MMC_BLK_STAT, false).trim().split("\\s+");
    }

    public String[] readSchedulers() {
        return shell.readCommand(MMC_BLK_SCHED, false).split("\\s+");
    }
}
