
package com.process;

public class UtilsInteractive {
    private static final String INTERACTIVE = "/sys/devices/system/cpu/cpufreq/interactive/";
    private static final String ABOVE_HISPEED_DELAY = INTERACTIVE + "above_hispeed_deplay";
    private static final String BOOST = INTERACTIVE + "boost";
    private static final String BOOSTPULSE = INTERACTIVE + "boostpulse";
    private static final String GO_HISPEED_LOAD = INTERACTIVE + "go_hispeed_load";
    private static final String HISPEED_FREQ = INTERACTIVE + "hispeed_freq";
    private static final String INPUT_BOOST = INTERACTIVE + "input_boost";
    private static final String MIN_SAMPLE_TIME = INTERACTIVE + "min_sample_time";
    private static final String TIMER_RATE = INTERACTIVE + "timer_rate";

    private final Shell shell;
    public final int[] settings;
    public static final int GO_HISPEED_LOAD_ORIG = 0;
    public static final int HISPEED_FREQ_ORIG = 1;
    public boolean isEncountered = false;

    public UtilsInteractive(Shell shell) {
        this.shell = shell;
        settings = new int[2];
    }

    public void setSettings() {
        long uid = android.os.Process.myPid();
        StringBuilder sb = new StringBuilder();
        Shell.changeMod(sb, INTERACTIVE + "*");
        Shell.changeOwner(sb, uid, INTERACTIVE + "*");
        shell.writeCommandSysFs(sb.toString());

        if (!isEncountered) {
            settings[GO_HISPEED_LOAD_ORIG] = this.getGoHighSpeedLoad();
            settings[HISPEED_FREQ_ORIG] = this.getHighSpeedFreq();
            isEncountered = true;
        }
    }

    public int getAboveHiSpeedDelay() {
        return Integer.parseInt(shell.readCommand(ABOVE_HISPEED_DELAY, false));
    }

    public int getBoost() {
        return Integer.parseInt(shell.readCommand(BOOST, false));
    }

    public int getBoostPulse() {
        return Integer.parseInt(shell.readCommand(BOOSTPULSE, false));
    }

    public int getGoHighSpeedLoad() {
        return Integer.parseInt(shell.readCommand(GO_HISPEED_LOAD, false));
    }

    public int getHighSpeedFreq() {
        return Integer.parseInt(shell.readCommand(HISPEED_FREQ, false));
    }

    public int getInputBoost() {
        return Integer.parseInt(shell.readCommand(INPUT_BOOST, false));
    }

    public int getMinSampleTime() {
        return Integer.parseInt(shell.readCommand(MIN_SAMPLE_TIME, false));
    }

    public int getTimerRate() {
        return Integer.parseInt(shell.readCommand(TIMER_RATE, false));
    }

    public void setAboveHiSpeedDelay(int value) {
        shell.writeCommand(value, ABOVE_HISPEED_DELAY);
    }

    public void setBoost(int value) {
        shell.writeCommand(value, BOOST);
    }

    public void setBoostPulse(int value) {
        shell.writeCommand(value, BOOSTPULSE);
    }

    public void setGoHighSpeedLoad(int value) {
        shell.writeCommand(value, GO_HISPEED_LOAD);
    }

    public void setHighSpeedFreq(int value) {
        shell.writeCommand(value, HISPEED_FREQ);
    }

    public void setInputBoost(int value) {
        shell.writeCommand(value, INPUT_BOOST);
    }

    public void setMinSampleTime(int value) {
        shell.writeCommand(value, MIN_SAMPLE_TIME);
    }

    public void setTimerRate(int value) {
        shell.writeCommand(value, TIMER_RATE);
    }

    public void reset() {
        setGoHighSpeedLoad(settings[GO_HISPEED_LOAD_ORIG]);
        setHighSpeedFreq(settings[HISPEED_FREQ_ORIG]);
    }

}
