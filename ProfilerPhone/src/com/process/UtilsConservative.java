package com.process;

public class UtilsConservative {

	private static final String CONSERVATIVE = "/sys/devices/system/cpu/cpufreq/conservative/";
	private static final String DOWN_THRESHOLD = CONSERVATIVE
			+ "down_threshold";
	private static final String FREQ_STEP = CONSERVATIVE + "freq_step";
	private static final String IGNORE_NICE_LOAD = CONSERVATIVE
			+ "ignore_nice_load";
	private static final String SAMPLING_DOWN_FACTOR = CONSERVATIVE
			+ "sampling_down_factor";
	private static final String SAMPLING_RATE = CONSERVATIVE + "sampling_rate";
	private static final String SAMPLING_RATE_MIN = CONSERVATIVE
			+ "sampling_rate_min";
	private static final String UP_THRESHOLD = CONSERVATIVE + "up_threshold";

	private final Shell shell;

	public static final int DOWN_THRESHOLD_ORIG = 0;
	public static final int UP_THRESHOLD_ORIG = 1;
	public static final int FREQ_STEP_ORIG = 2;
	public int[] settings;
	public boolean isEncountered = false;

	public UtilsConservative(Shell shell) {
		this.shell = shell;
		settings = new int[3];
	}

	public void setSettings() {
		if (!isEncountered) {
			settings[DOWN_THRESHOLD_ORIG] = getDownThreshold();
			settings[UP_THRESHOLD_ORIG] = getUpThreshold();
			settings[FREQ_STEP_ORIG] = getFreqStep();
			isEncountered = true;
		}
	}

	public int getDownThreshold() {
		return Integer.parseInt(shell.readCommand(DOWN_THRESHOLD, false));
	}

	public int getFreqStep() {
		return Integer.parseInt(shell.readCommand(FREQ_STEP, false));
	}

	public int getIgnoreNiceLoad() {
		return Integer.parseInt(shell.readCommand(IGNORE_NICE_LOAD, false));
	}

	public int getSamplingDownFactor() {
		return Integer.parseInt(shell.readCommand(SAMPLING_DOWN_FACTOR, false));
	}

	public int getSamplingRate() {
		return Integer.parseInt(shell.readCommand(SAMPLING_RATE, false));
	}

	public int getSamplingRateMin() {
		return Integer.parseInt(shell.readCommand(SAMPLING_RATE_MIN, false));
	}

	public int getUpThreshold() {
		return Integer.parseInt(shell.readCommand(UP_THRESHOLD, false));
	}

	public void setDownThreshold(int value) {
		shell.writeCommand(value, DOWN_THRESHOLD);
	}

	public void setFreqStep(int value) {
		shell.writeCommand(value, FREQ_STEP);
	}

	public void setIgnoreNiceLoad(int value) {
		shell.writeCommand(value, IGNORE_NICE_LOAD);
	}

	public void setSamplingDownFactor(int value) {
		shell.writeCommand(value, SAMPLING_DOWN_FACTOR);
	}

	public void setSamplingRate(int value) {
		shell.writeCommand(value, SAMPLING_RATE);
	}

	public void setSamplingRateMin(int value) {
		shell.writeCommand(value, SAMPLING_RATE_MIN);
	}

	public void setUpThreshold(int value) {
		shell.writeCommand(value, UP_THRESHOLD);
	}

	public void reset() {
		setDownThreshold(settings[DOWN_THRESHOLD_ORIG]);
		setUpThreshold(settings[UP_THRESHOLD_ORIG]);
		setFreqStep(settings[FREQ_STEP_ORIG]);
	}

}
