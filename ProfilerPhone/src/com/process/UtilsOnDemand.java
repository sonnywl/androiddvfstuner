
package com.process;

public class UtilsOnDemand {

    private static final String ONDEMAND = "/sys/devices/system/cpu/cpufreq/ondemand/";

    /**
     * ignore_nice_load: this parameter takes a value of '0' or '1'. When set to
     * '0' (its default), all processes are counted towards the 'cpu
     * utilisation' value. When set to '1', the processes that are run with a
     * 'nice' value will not count (and thus be ignored) in the overall usage
     * calculation. This is useful if you are running a CPU intensive
     * calculation on your laptop that you do not care how long it takes to
     * complete as you can 'nice' it and prevent it from taking part in the
     * deciding process of whether to increase your CPU frequency.
     */
    private static final String IGNORE_NICE_LOAD = ONDEMAND + "ignore_nice_load";
    private static final String IO_IS_BUSY = ONDEMAND + "io_is_busy";
    /**
     * powersave_bias: this parameter takes a value between 0 to 1000. It
     * defines the percentage (times 10) value of the target frequency that will
     * be shaved off of the target. For example, when set to 100 -- 10%, when
     * ondemand governor would have targeted 1000 MHz, it will target 1000 MHz -
     * (10% of 1000 MHz) = 900 MHz instead. This is set to 0 (disabled) by
     * default.
     */
    private static final String POWERSAVE_BIAS = ONDEMAND + "powersave_bias";
    /**
     * sampling_down_factor: this parameter controls the rate at which the
     * kernel makes a decision on when to decrease the frequency while running
     * at top speed. When set to 1 (the default) decisions to reevaluate load
     * are made at the same interval regardless of current clock speed. But when
     * set to greater than 1 (e.g. 100) it acts as a multiplier for the
     * scheduling interval for reevaluating load when the CPU is at its top
     * speed due to high load. This improves performance by reducing the
     * overhead of load evaluation and helping the CPU stay at its top speed
     * when truly busy, rather than shifting back and forth in speed. This
     * tunable has no effect on behavior at lower speeds/lower CPU loads.
     */
    private static final String SAMPLING_DOWN_FACTOR = ONDEMAND + "sampling_down_factor";
    /**
     * sampling_rate: measured in uS (10^-6 seconds), this is how often you want
     * the kernel to look at the CPU usage and to make decisions on what to do
     * about the frequency. Typically this is set to values of around '10000' or
     * more. It's default value is (cmp. with users-guide.txt):
     * transition_latency * 1000 Be aware that transition latency is in ns and
     * sampling_rate is in us, so you get the same sysfs value by default.
     * Sampling rate should always get adjusted considering the transition
     * latency To set the sampling rate 750 times as high as the transition
     * latency in the bash (as said, 1000 is default), do: echo `$(($(cat
     * cpuinfo_transition_latency) * 750 / 1000)) \ >ondemand/sampling_rate
     */
    private static final String SAMPLING_RATE = ONDEMAND + "sampling_rate";
    /**
     * sampling_rate_min: The sampling rate is limited by the HW transition
     * latency: transition_latency * 100 Or by kernel restrictions: If
     * CONFIG_NO_HZ_COMMON is set, the limit is 10ms fixed. If
     * CONFIG_NO_HZ_COMMON is not set or nohz=off boot parameter is used, the
     * limits depend on the CONFIG_HZ option: HZ=1000: min=20000us (20ms)
     * HZ=250: min=80000us (80ms) HZ=100: min=200000us (200ms) The highest value
     * of kernel and HW latency restrictions is shown and used as the minimum
     * sampling rate.
     */
    private static final String SAMPLING_RATE_MIN = ONDEMAND + "sampling_rate_min";
    /**
     * up_threshold: defines what the average CPU usage between the samplings of
     * 'sampling_rate' needs to be for the kernel to make a decision on whether
     * it should increase the frequency. For example when it is set to its
     * default value of '95' it means that between the checking intervals the
     * CPU needs to be on average more than 95% in use to then decide that the
     * CPU frequency needs to be increased.
     */
    private static final String UP_THRESHOLD = ONDEMAND + "up_threshold";

    private static final String DOWN_DIFFERENTIAL = ONDEMAND + "down_differential";

    private final Shell shell;

    public static final int SAMPLING_RATE_ORIG = 0;
    public static final int UP_THRESHOLD_ORIG = 1;
    public static final int SAMPLING_DOWN_FACTOR_ORIG = 2;
    public static final int POWERSAVE_BIAS_ORIG = 3;

    public int[] settings;
    public int[] currSettings;
    public boolean isEncountered = false;

    public UtilsOnDemand(Shell shell) {
        this.shell = shell;
        settings = new int[4];
        currSettings = new int[4];
    }

    public void setSettings() {
        long uid = android.os.Process.myPid();
        StringBuilder sb = new StringBuilder();
        Shell.changeMod(sb, ONDEMAND + "*");
        Shell.changeOwner(sb, uid, ONDEMAND + "*");
        shell.writeCommandSysFs(sb.toString());

        if (!isEncountered) {
            settings[UP_THRESHOLD_ORIG] = 95;
            settings[SAMPLING_RATE_ORIG] = 300000;
            settings[SAMPLING_DOWN_FACTOR_ORIG] = 1;
            settings[POWERSAVE_BIAS_ORIG] = 0;
            currSettings[UP_THRESHOLD_ORIG] = settings[UP_THRESHOLD_ORIG];
            currSettings[SAMPLING_RATE_ORIG] = settings[SAMPLING_RATE_ORIG];
            currSettings[SAMPLING_DOWN_FACTOR_ORIG] = settings[SAMPLING_DOWN_FACTOR_ORIG];
            currSettings[POWERSAVE_BIAS_ORIG] = settings[POWERSAVE_BIAS_ORIG];
            isEncountered = true;
        }
    }

    public int getIgnoreNiceLoad() {
        return Integer.parseInt(shell.readCommand(IGNORE_NICE_LOAD, false));
    }

    public int getIOIsBusy() {
        return Integer.parseInt(shell.readCommand(IO_IS_BUSY, false));
    }

    public int getPowerSaveBias() {
        return Integer.parseInt(shell.readCommand(POWERSAVE_BIAS, false));
    }

    public int getSamplingDownFactor() {
        return Integer.parseInt(shell.readCommand(SAMPLING_DOWN_FACTOR, false));
    }

    public int getSamplingRate() {
        return Integer.parseInt(shell.readCommand(SAMPLING_RATE, false));
    }

    public int getSamplingRateMin() {
        return Integer.parseInt(shell.readCommand(SAMPLING_RATE_MIN, false));
    }

    public int getUpThreshold() {
        return Integer.parseInt(shell.readCommand(UP_THRESHOLD, false));
    }
    
    public int getDownDifferential() {
        return Integer.parseInt(shell.readCommand(DOWN_DIFFERENTIAL, false));
    }

    public void setPowerSaveBias(int value) {
        if (value <= 1000 && value >= 0) {
            if (currSettings[POWERSAVE_BIAS_ORIG] != value) {
                currSettings[POWERSAVE_BIAS_ORIG] = value;
                // shell.writeCommandSysFs("echo " + value + " > " +
                // POWERSAVE_BIAS);
                shell.writeCommand(value, POWERSAVE_BIAS);
            }
        }
    }

    public void setSamplingRate(int value) {
        if (currSettings[SAMPLING_RATE_ORIG] != value) {
            currSettings[SAMPLING_RATE_ORIG] = value;
            // shell.writeCommandSysFs("echo " + value + " > " + SAMPLING_RATE);
            shell.writeCommand(value, SAMPLING_RATE);
        }
    }

    public void setSamplingDownFactor(int value) {
        if (value < 10) {
            if (currSettings[SAMPLING_DOWN_FACTOR_ORIG] != value) {
                currSettings[SAMPLING_DOWN_FACTOR_ORIG] = value;
                // shell.writeCommandSysFs("echo " + value + " > " +
                // SAMPLING_DOWN_FACTOR);
                shell.writeCommand(value, SAMPLING_DOWN_FACTOR);
            }
        }
    }

    public void setUpThreshold(int value) {
        if (currSettings[UP_THRESHOLD_ORIG] != value) {
            currSettings[UP_THRESHOLD_ORIG] = value;
            // shell.writeCommandSysFs("echo " + value + " > " + UP_THRESHOLD +
            // "\n");
            shell.writeCommand(value, UP_THRESHOLD);
        }
    }
    
    public void setDownDifferntial(int value) {
        shell.writeCommand(value, DOWN_DIFFERENTIAL);
    }

    public void reset() {
        setUpThreshold(settings[UP_THRESHOLD_ORIG]);
        setSamplingRate(settings[SAMPLING_RATE_ORIG]);
        setSamplingDownFactor(settings[SAMPLING_DOWN_FACTOR_ORIG]);
        setPowerSaveBias(settings[POWERSAVE_BIAS_ORIG]);
    }
}
