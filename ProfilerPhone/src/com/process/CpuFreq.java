package com.process;

public interface CpuFreq {
	public String[] getAvailableFrequencies(int cpu);

	public String[] getAvailableGovernors(int cpu);

	public String getCurGovernor(int cpu);

	public int getCurFreq(int cpu);

	public int getMaxFreq(int cpu);

	public int getMinFreq(int cpu);

	public void setMaxFreq(int cpu, int freq);

	public void setMinFreq(int cpu, int freq);

	public void setCurFreq(int cpu, int freq);
}
