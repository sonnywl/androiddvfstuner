
package com.process;

import android.util.Log;
import android.util.SparseArray;

import com.profiler.services.ProfilerService;

public class UtilsCpu extends Thread implements CpuFreq {
    private final static String TAG = "UtilsCpu";

    public final static String STATS = "/proc/stat";
    public final static String AVAILABLE_CPU = "/sys/devices/system/cpu/possible";
    public final static String CPUFS = "/sys/devices/system/cpu/";
    private final static String CPUINFO_CUR_FREQ = "/cpufreq/cpuinfo_cur_freq";
    private final static String CPUINFO_MAX_FREQ = "/cpufreq/cpuinfo_max_freq";
    private final static String CPUINFO_MIN_FREQ = "/cpufreq/cpuinfo_min_freq";
    private final static String SCALING_CUR_FREQ = "/cpufreq/scaling_cur_freq";
    private final static String SCALING_MIN_FREQ = "/cpufreq/scaling_min_freq";
    private final static String SCALING_MAX_FREQ = "/cpufreq/scaling_max_freq";
    @SuppressWarnings("unused")
    /** Not Exactly accurate */
    private final static String SCALING_CPU_FREQ = "/cpufreq/scaling_cur_freq";
    private final static String SCALING_GOVERNOR = "/cpufreq/scaling_governor";
    private final static String SCALING_AVAILABLE_FREQ = "/cpufreq/scaling_available_frequencies";
    private final static String SCALING_AVAILABLE_GOVERNORS = "/cpufreq/scaling_available_governors";
    private final static String SCALING_STATS_TIME_IN_STATE = "/cpufreq/stats/time_in_state";
    private final static String SCALING_STATS_TOTAL_TRANS = "/cpufreq/stats/total_trans";
    private static StringBuilder sb = new StringBuilder();
    private final Shell shell;
    private static SparseArray<String> CPU_FREQ_CUR;
    private static SparseArray<String> CPU_FREQ_MIN;
    private static SparseArray<String> CPU_FREQ_MAX;
    private static SparseArray<String> SCALING_CUR;
    private static SparseArray<String> SCALING_MAX;
    private static SparseArray<String> SCALING_MIN;
    private static SparseArray<String> SCALING_GOVERNORS;
    private static SparseArray<String> SCALING_AVAIL_GOVERNORS;
    private static SparseArray<String> SCALING_AVAIL_CPU_FREQS;
    private static SparseArray<String> SCALING_STATS_TIME_IN_STATES;
    private static SparseArray<String> SCALING_STATS_TOTAL_TRANSITIONS;
    private static UtilsCpu INSTANCE;

    private static int availableCpus;
    private static int[] availableFrequencies;
    private static String[] availableGovernors;
    public UtilsInteractive interactive;
    public UtilsConservative conservative;
    public UtilsOnDemand ondemand;

    private static String flushStringBuilder() {
        String result = sb.toString();
        sb.setLength(0);
        return result;
    }

    public static int getAvailableCpus() {
        return availableCpus;
    }

    public static int[] getAvailableFrequencies() {
        return availableFrequencies;
    }

    public static String[] getAvailableGovernors() {
        return availableGovernors;
    }

    private static String getCpu(int cpu) {
        if (cpu < availableCpus) {
            return "cpu" + cpu;
        }
        Log.i(TAG, "unknown cpu returning cpu0 instead of cpu" + cpu);
        return "cpu0";
    }

    private static String[] getStringArray(String msg, String splitPattern) {
        if (msg != null) {
            return msg.split(splitPattern);
        }
        return new String[] {};
    }

    public static int getTotalCpus() {
        return availableCpus;
    }

    public static UtilsCpu newInstance(final Shell shell, final ProfilerService service) {
        if (INSTANCE != null) {
            return INSTANCE;
        }
        String availableCpu = shell.readCommand(AVAILABLE_CPU, false).trim();
        availableCpus = Character.getNumericValue(availableCpu.charAt(availableCpu.length() - 1)) + 1;
        CPU_FREQ_CUR = new SparseArray<String>(availableCpus);
        CPU_FREQ_MIN = new SparseArray<String>(availableCpus);
        CPU_FREQ_MAX = new SparseArray<String>(availableCpus);
        SCALING_CUR = new SparseArray<String>(availableCpus);
        SCALING_MAX = new SparseArray<String>(availableCpus);
        SCALING_MIN = new SparseArray<String>(availableCpus);
        SCALING_GOVERNORS = new SparseArray<String>(availableCpus);
        SCALING_AVAIL_GOVERNORS = new SparseArray<String>(availableCpus);
        SCALING_AVAIL_CPU_FREQS = new SparseArray<String>(availableCpus);
        SCALING_STATS_TIME_IN_STATES = new SparseArray<String>(availableCpus);
        SCALING_STATS_TOTAL_TRANSITIONS = new SparseArray<String>(availableCpus);
        for (int cpu = 0; cpu < availableCpus; cpu++) {
            sb.append(CPUFS).append(getCpu(cpu)).append(CPUINFO_CUR_FREQ);
            CPU_FREQ_CUR.put(cpu, flushStringBuilder());
            sb.append(CPUFS).append(getCpu(cpu)).append(CPUINFO_MAX_FREQ);
            CPU_FREQ_MAX.put(cpu, flushStringBuilder());
            sb.append(CPUFS).append(getCpu(cpu)).append(CPUINFO_MIN_FREQ);
            CPU_FREQ_MIN.put(cpu, flushStringBuilder());
            sb.append(CPUFS).append(getCpu(cpu)).append(SCALING_CUR_FREQ);
            SCALING_CUR.put(cpu, flushStringBuilder());
            sb.append(CPUFS).append(getCpu(cpu)).append(SCALING_MAX_FREQ);
            SCALING_MAX.put(cpu, flushStringBuilder());
            sb.append(CPUFS).append(getCpu(cpu)).append(SCALING_MIN_FREQ);
            SCALING_MIN.put(cpu, flushStringBuilder());
            sb.append(CPUFS).append(getCpu(cpu)).append(SCALING_GOVERNOR);
            SCALING_GOVERNORS.put(cpu, flushStringBuilder());
            sb.append(CPUFS).append(getCpu(cpu)).append(SCALING_AVAILABLE_GOVERNORS);
            SCALING_AVAIL_GOVERNORS.put(cpu, flushStringBuilder());
            sb.append(CPUFS).append(getCpu(cpu)).append(SCALING_AVAILABLE_FREQ);
            SCALING_AVAIL_CPU_FREQS.put(cpu, flushStringBuilder());
            sb.append(CPUFS).append(getCpu(cpu)).append(SCALING_STATS_TIME_IN_STATE);
            SCALING_STATS_TIME_IN_STATES.put(cpu, flushStringBuilder());
            sb.append(CPUFS).append(getCpu(cpu)).append(SCALING_STATS_TOTAL_TRANS);
            SCALING_STATS_TOTAL_TRANSITIONS.put(cpu, flushStringBuilder());
        }
        long uid = android.os.Process.myPid();
        for (int cpu = 0; cpu < availableCpus; cpu++) {
            Shell.changeOwner(sb, uid, CPU_FREQ_CUR.get(cpu));
            Shell.changeOwner(sb, uid, CPU_FREQ_MAX.get(cpu));
            Shell.changeOwner(sb, uid, CPU_FREQ_MIN.get(cpu));
            Shell.changeOwner(sb, uid, SCALING_CUR.get(cpu));
            Shell.changeOwner(sb, uid, SCALING_MAX.get(cpu));
            Shell.changeOwner(sb, uid, SCALING_MIN.get(cpu));
            Shell.changeOwner(sb, uid, SCALING_GOVERNORS.get(cpu));
            Shell.changeOwner(sb, uid, SCALING_AVAIL_CPU_FREQS.get(cpu));
            Shell.changeOwner(sb, uid, SCALING_AVAIL_GOVERNORS.get(cpu));
            Shell.changeMod(sb, CPU_FREQ_CUR.get(cpu));
            Shell.changeMod(sb, CPU_FREQ_MAX.get(cpu));
            Shell.changeMod(sb, CPU_FREQ_MIN.get(cpu));
            Shell.changeMod(sb, SCALING_CUR.get(cpu));
            Shell.changeMod(sb, SCALING_MAX.get(cpu));
            Shell.changeMod(sb, SCALING_MIN.get(cpu));
            Shell.changeMod(sb, SCALING_GOVERNORS.get(cpu));
            Shell.changeMod(sb, SCALING_AVAIL_CPU_FREQS.get(cpu));
            Shell.changeMod(sb, SCALING_AVAIL_GOVERNORS.get(cpu));
            shell.writeCommandSysFs(flushStringBuilder());
        }
        INSTANCE = new UtilsCpu(shell);
        return INSTANCE;
    }

    public void switchCpuGovernor(String governor) {
        for (int i = 0; i < availableGovernors.length; i++) {
            if (governor.equals(availableGovernors[i])) {
                break;
            }
        }
        sb.setLength(0);
        for (int i = 0; i < availableCpus; i++) {
            sb.append("echo " + governor + " > " + SCALING_GOVERNORS.get(i) + "\n");
        }
        String swap = sb.toString();
        sb.setLength(0);
        shell.writeCommandSysFs(swap);
    }

    public static void setAvailableCpus(int availableCpus) {
        UtilsCpu.availableCpus = availableCpus;
    }

    public static void setAvailableFrequencies(int[] availableFrequencies) {
        UtilsCpu.availableFrequencies = availableFrequencies;
    }

    public static void setAvailableGovernors(String[] availableGovernors) {
        UtilsCpu.availableGovernors = availableGovernors;
    }

    private UtilsCpu(Shell shellInstance) {
        shell = shellInstance;
        String[] freq = getAvailableFrequencies(0);
        int[] frequencies = new int[freq.length];
        for (int i = 0; i < freq.length; i++) {
            frequencies[i] = Integer.parseInt(freq[i]);
        }
        setAvailableFrequencies(frequencies);
        setAvailableGovernors(getAvailableGovernors(0));
        interactive = new UtilsInteractive(shell);
        conservative = new UtilsConservative(shell);
        ondemand = new UtilsOnDemand(shell);
    }

    public String getAvailableCpu() {
        return shell.readCommand(AVAILABLE_CPU, false);
    }

    @Override
    public String[] getAvailableFrequencies(int cpu) {
        String frequencies = shell.readCommand(SCALING_AVAIL_CPU_FREQS.get(cpu), true).trim();
        return getStringArray(frequencies, " ");
    }

    @Override
    public String[] getAvailableGovernors(int cpu) {
        String governors = shell.readCommand(SCALING_AVAIL_GOVERNORS.get(cpu), false);
        return getStringArray(governors, " ");
    }

    @Override
    public int getCurFreq(int cpu) {
        return Integer.valueOf(shell.readCommand(SCALING_CUR.get(cpu), false));
    }

    @Override
    public String getCurGovernor(int cpu) {
        return shell.readCommand(SCALING_GOVERNORS.get(cpu), false);
    }

    @Override
    public int getMaxFreq(int cpu) {
        return Integer.valueOf(shell.readCommand(SCALING_MAX.get(cpu), false));
    }

    @Override
    public int getMinFreq(int cpu) {
        return Integer.valueOf(shell.readCommand(SCALING_MIN.get(cpu), false));
    }

    public String getStats() {
        return shell.readCommand(STATS, true);
    }

    public String[] getTimeInState(int cpu) {
        String timeState = shell.readCommand(SCALING_STATS_TIME_IN_STATES.get(cpu), true).trim();
        return getStringArray(timeState, "/\n");
    }

    public long getTotalTransition(int cpu) {
        return Long.parseLong(shell.readCommand(SCALING_STATS_TOTAL_TRANSITIONS.get(cpu), false));
    }

    public void setCurGovernor(int cpu, String governor) {
        shell.writeCommand(governor, SCALING_GOVERNORS.get(cpu));
    }

    @Override
    public void setCurFreq(int cpu, int freq) {
        shell.writeCommand(freq, SCALING_CUR.get(cpu));
    }

    @Override
    public void setMaxFreq(int cpu, int freq) {
        shell.writeCommand(freq, SCALING_MAX.get(cpu));
    }

    @Override
    public void setMinFreq(int cpu, int freq) {
        shell.writeCommand(freq, SCALING_MIN.get(cpu));
    }
}
