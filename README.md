# Android Application Level DVFS Tuner #



### What is this repository for? ###
This repository contains the projects used for the master thesis "Android Application Level CPU DVFS Tuner" 

### How do I get set up? ###
The repository contains the following projects:
BenchmarkNativeTest contains the modified mibench tests for our constant repetition tests

ProfilerAutomation contains the AutoIt3 scripts to control the benchmark tests and batch files used to call the MonkeyRunner scripts

ProfilerPhone is Android DVFS Tuner project for the Android devices.


BenchmarkNativeTest and ProfilerPhone are Eclipse Android projects. Importing and building them with an Android 3.0 SDK or higher will work.

ProfilerAutomation is an automation script built to execute MonkeyRunner.
This is more of an example of automated testing. It requires the absolute path of your program and MonkeyRunner to properly execute the python and batch scripts. In order to gather the data from Monsoon Power Monitor, the power monitor has to be activated and running on the Windows OS. The program starts by Benchmark.au3 which contains a mapping of the possible tests to execute with their elapsed times.

### Contact ###
Please feel free to contact me via my email at time_current "at" hotmail "dot" com for more information.