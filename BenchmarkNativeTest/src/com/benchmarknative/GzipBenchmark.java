package com.benchmarknative;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.util.zip.GZIPInputStream;
import java.util.zip.GZIPOutputStream;

public class GzipBenchmark {
	private int gzipFileSize;

	public GzipBenchmark(int size) {
		gzipFileSize = size / 2;
	}

	public byte[] compress() {
		StringBuilder sb = new StringBuilder(gzipFileSize / 2);
		int counter = 0;
		while (counter < gzipFileSize) {
			sb.append("a");
			counter++;
		}
		byte[] fileSize = sb.toString().getBytes();
		ByteArrayOutputStream boas = new ByteArrayOutputStream();
		try {
			GZIPOutputStream gos = new GZIPOutputStream(boas);
			gos.write(fileSize);
			gos.flush();
			gos.close();
			boas.close();
			return boas.toByteArray();
		} catch (IOException e) {
			e.printStackTrace();
		} finally {
			try {
				boas.close();
			} catch (IOException e) {
				e.printStackTrace();
			}

		}
		return null;
	}
	
	public String decompress(byte[] compressed) {
		String value = null;
	    final int BUFFER_SIZE = 32;
		try {
			ByteArrayInputStream bais = new ByteArrayInputStream(compressed);
			GZIPInputStream gis = new GZIPInputStream(bais);
		    StringBuilder string = new StringBuilder();
		    byte[] data = new byte[BUFFER_SIZE];
		    int bytesRead;
		    while ((bytesRead = gis.read(data)) != -1) {
		        string.append(new String(data, 0, bytesRead));
		    }
		    gis.close();
		    bais.close();
		    return string.toString();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return value;
	}
}
