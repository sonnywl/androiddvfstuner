
package com.benchmarknative;

import android.app.Activity;
import android.os.Bundle;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.PopupMenu;
import android.widget.PopupMenu.OnMenuItemClickListener;
import android.widget.TextView;

import com.lib.CircularDoubleArray;

import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.RandomAccessFile;
import java.text.DecimalFormat;
import java.util.ArrayList;

public class MainActivity extends Activity implements OnMenuItemClickListener, OnClickListener {
    static final String TAG = "MainBenchmark";
    private PopupMenu popBenchMarkSelection;
    private TextView benchmarkSelection;
    private TextView benchmarkRes;
    private Button btn_execute;
    private int mode = 0;
    public static final DecimalFormat df = new DecimalFormat("#.##");
    private long[] prevIdle;
    private long[] prevTotal;
    private double[] utilization;
    private int totalCpus = 2;
    private CircularDoubleArray util;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        benchmarkSelection = (TextView) this.findViewById(R.id.benchmark_selection);
        benchmarkRes = (TextView) this.findViewById(R.id.benchmark_results);
        btn_execute = (Button) this.findViewById(R.id.benchmark_execute);
        benchmarkSelection.setText(R.string.basicmath_small);
        benchmarkSelection.setOnClickListener(this);
        btn_execute.setOnClickListener(this);
        generateMenu();

        prevIdle = new long[totalCpus + 1];
        prevTotal = new long[totalCpus + 1];
        utilization = new double[totalCpus + 1];
        util = new CircularDoubleArray(5);
    }

    private void generateMenu() {
        popBenchMarkSelection = new PopupMenu(this, benchmarkSelection);
        popBenchMarkSelection.getMenuInflater().inflate(R.menu.benchmark,
                popBenchMarkSelection.getMenu());
        popBenchMarkSelection.setOnMenuItemClickListener(this);
    }

    public void updateBenchmarkInfo(String msg) {
        benchmarkRes.setText(msg);
    }

    @Override
    public boolean onMenuItemClick(MenuItem item) {
        mode = item.getItemId();

        switch (mode) {
            default:
                // case R.id.matrix_multi:
                // benchmarkSelection.setText(R.string.matrix_multi);
                // break;
                // case R.id.download:
                // benchmarkSelection.setText(R.string.download);
                // break;
                // case R.id.io_read:
                // benchmarkSelection.setText(R.string.io_read);
                // break;
                // case R.id.io_write:
                // benchmarkSelection.setText(R.string.io_write);
                // break;
            case R.id.basicmath_large:
                benchmarkSelection.setText(R.string.basicmath_large);
                break;
            case R.id.basicmath_small:
                benchmarkSelection.setText(R.string.basicmath_small);
                break;
            case R.id.bitcnts_large:
                benchmarkSelection.setText(R.string.bitcnts_large);
                break;
            case R.id.bitcnts_small:
                benchmarkSelection.setText(R.string.bitcnts_small);
                break;
            case R.id.fft_large:
                benchmarkSelection.setText(R.string.fft_large);
                break;
            case R.id.fft_small:
                benchmarkSelection.setText(R.string.fft_small);
                break;
            case R.id.gzip_avg_2014:
                benchmarkSelection.setText(R.string.gzip_avg_2014);
                break;
            case R.id.gzip_5120:
                benchmarkSelection.setText(R.string.gzip_5120);
                break;
            case R.id.gzip_10240:
                benchmarkSelection.setText(R.string.gzip_10240);
                break;
        }
        return false;
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.benchmark_selection:
                popBenchMarkSelection.show();
                break;
            case R.id.benchmark_execute:
                int testType = 0;
                int interval = 150;
                switch (mode) {
                    default:
                        // case R.id.matrix_multi:
                        // break;
                        // case R.id.download:
                        // break;
                        // case R.id.io_read:
                        // break;
                        // case R.id.io_write:
                        // break;
                    case R.id.basicmath_small:
                        testType = 10;
                        break;
                    case R.id.basicmath_large:
                        testType = 11;
                        break;
                    case R.id.bitcnts_large:
                        testType = 22;
                        break;
                    case R.id.bitcnts_small:
                        testType = 23;
                        break;
                    case R.id.fft_large:
                        testType = 26;
                        interval = 50;
                        break;
                    case R.id.fft_small:
                        testType = 25;
                        interval = 50;
                        break;
                    case R.id.gzip_avg_2014:
//                        testType                     = 100;
                        interval = 10;
                        break;
                    case R.id.gzip_5120:
                        testType = 101;
                        interval = 0;
                        break;
                    case R.id.gzip_10240:
                        testType = 102;
                        interval = 0;
                        break;
                // case R.id.linpack:
                // testType = 12;
                // break;
                // case R.id.qsort:
                // testType = 13;
                // break;
                }
                double totalTime = 0d;
                StringBuilder sb = new StringBuilder();

                int SECS = 10 * 1000;
                int tests = 0;
                long startTime = System.currentTimeMillis();
                ArrayList<Double> times = new ArrayList<Double>();
                while (System.currentTimeMillis() - startTime < SECS || tests < 10) {
                    if (testType < 100) {
                        double time = Double.parseDouble(loadTest(testType));
                        times.add(time);
                        totalTime += time;
                    } else {
                        GzipBenchmark gb;
                        switch(testType) {
                            default:
                            case 100:
                                gb = new GzipBenchmark(1246000);
                                break;
                            case 101:
                                gb = new GzipBenchmark(5120000);
                                break;
                            case 102:
                                gb = new GzipBenchmark(10240000);
                                break;
                        }
                        long testStartTime = System.currentTimeMillis();
                        gb.compress();
                        long testEndTime = System.currentTimeMillis() - testStartTime;
                        times.add((double) testEndTime);
                        totalTime += testEndTime;
                    }
                    try {
                        Thread.sleep(interval);
                    } catch (InterruptedException e) {
                        e.printStackTrace();
                    }

                    tests++;
                    util.add(getCpuUtilization());
                    Log.i("MAIN",
                            "Util " + df.format(util.getAverage()) + " "
                                    + df.format(utilization[0]) + " " + df.format(utilization[1])
                                    + " " + df.format(utilization[2]));
                }
                long endTime = System.currentTimeMillis() - startTime;
                sb.append("Benchmark:\nTotal Time ").append(df.format(totalTime));
                sb.append("\nElapsed Time ").append(endTime / 1000);
                sb.append("\nTest Taken ").append(tests);
                sb.append("\nAvg Test Time ").append(totalTime / tests);

                updateBenchmarkInfo(sb.toString());
                Log.i(TAG, "Benchmark:" + df.format(totalTime));
                break;
        }
    }

    private double getCpuUtilization() {
        RandomAccessFile cpuInfo = null;
        try {
            // monitor total and idle cpu stat of certain process
            cpuInfo = new RandomAccessFile("/proc/stat", "r");
            for (int i = 0; i < totalCpus + 1; i++) {
                String[] toks = cpuInfo.readLine().split("\\s+");

                long idleCpu = Long.parseLong(toks[4]);
                long totalCpu = Long.parseLong(toks[1]) + Long.parseLong(toks[2])
                        + Long.parseLong(toks[3]) + Long.parseLong(toks[5])
                        + Long.parseLong(toks[4]) + Long.parseLong(toks[6])
                        + Long.parseLong(toks[7]);

                long diffIdleCpu = idleCpu - prevIdle[i];
                long diffTotalCpu = totalCpu - prevTotal[i];
                double usage = 1d;
                try {
                    usage = 100 * ((diffTotalCpu - diffIdleCpu) * 1.0 / diffTotalCpu);
                } catch (Exception e) {
                    usage = 100 * ((diffTotalCpu - diffIdleCpu) * 1.0 / 1);
                }
                prevIdle[i] = idleCpu;
                prevTotal[i] = totalCpu;
                utilization[i] = usage;
            }

        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        } finally {
            if (cpuInfo != null) {
                try {
                    cpuInfo.close();
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        }
        return utilization[0];
    }

    public native String loadTest(int state);

    public void updateFromJni(String msg) {

    }

    static {
        System.loadLibrary("benchmark");
    }
}
