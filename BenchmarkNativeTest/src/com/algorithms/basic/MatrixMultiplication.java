package com.algorithms.basic;

import android.os.AsyncTask;

import com.benchmarknative.MainActivity;

public class MatrixMultiplication extends AsyncTask<String, Void, String> {
	static String TAG = "MatrixMultiplication";
	int[][] A;
	int[][] B;
	int[][] C;
	int size;
	int iterations;
	int sleep;
	final MainActivity mainActivity;

	public MatrixMultiplication(MainActivity mainActivity, int iterations,
			int sleep, int size) {
		this.size = size;
		this.iterations = iterations;
		this.sleep = sleep;
		this.mainActivity = mainActivity;
		A = new int[size][size];
		B = new int[size][size];
		for (int i = 0; i < A.length; i++) {
			for (int j = 0; j < A.length; j++) {
				A[i][j] = i + 10;
				B[i][j] = j + 10;
			}
		}
	}

	@Override
	protected String doInBackground(String... params) {
		long time = System.currentTimeMillis();
		StringBuilder sb = new StringBuilder();
		int[][] C = new int[A.length][A[0].length];
		for (int g = 0; g < iterations; g++) {
			for (int i = 0; i < A.length; i++) {
				for (int j = 0; j < B.length; j++) {
					for (int k = 0; k < B.length; k++) {
						C[i][j] += A[i][k] * B[k][j];
					}
				}
			}
			try {
				Thread.sleep(sleep);
			} catch (InterruptedException e) {
				e.printStackTrace();
			}
		}
		sb.append("Time Elapsed " + (System.currentTimeMillis() - time));
		return sb.toString();
	}
}
