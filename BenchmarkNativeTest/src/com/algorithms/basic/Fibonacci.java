package com.algorithms.basic;
public class Fibonacci {

	/**
	 * @param args
	 */
//	public static void main(String[] args) {
//		Fibonacci f = new Fibonacci();
//		f.run(10);
//	}

	public int run(int value) {
		if (value == 0)
			return 0;
		if (value == 1)
			return 1;
		return run(value - 1) + run(value - 2);
	}

}
