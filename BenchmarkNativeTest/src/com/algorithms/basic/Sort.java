package com.algorithms.basic;
public class Sort {

	int array[], orig[];

//	public static void main(String[] args) {
//		int[] test = { 15, 2, 45, 3, 6, 9, 100 };
//		Sort s = new Sort(test);
//		s.insertionSort();
//		s.printArray();
//		s.reset();
//		// s.selectionSort();
//		// s.printArray();
//		s.quickSort();
//		s.printArray();
//		// s.reset();
//		// s.mergeSort();
//		// s.printArray();
//	}

	public Sort(int[] sortArray) {
		orig = new int[sortArray.length];
		for (int i = 0; i < sortArray.length; ++i)
			orig[i] = sortArray[i];
		array = sortArray;
	}

	public void quickSort() {
		qSort(0, array.length - 1);
	}

	private void qSort(int start, int end) {
		if (start < end) {
			int splitpoint = split(start, end);
			if (start < splitpoint - 1)
				qSort(start, splitpoint - 1);
			if (splitpoint < end)
				qSort(splitpoint, end);
		}
	}

	private int split(int start, int end) {
		int tmp;
		int pivot = array[(start + end) / 2];
		while (start <= end) {
			while (array[start] < pivot)
				start++;
			while (array[end] > pivot)
				end--;

			if (start <= end) {
				tmp = array[start];
				array[start] = array[end];
				array[end] = tmp;
				start++;
				end--;
			}
		}
		return start;
	}

	public void mergeSort() {
		mSort(0, array.length - 1);
	}

	private void mSort(int first, int last) {
		if (first >= last)
			return;
		int mid = (first + last) / 2;
		mSort(first, mid);
		mSort(mid + 1, last);
		merge(first, last);
	}

	private void merge(int first, int last) {
		int tmp[] = new int[last - first + 1];
		int mid = (first + last) / 2;
		int i = first;
		int j = mid;
		int k = 0;
		while (i < mid && j < last) {
			if (array[i] < array[j])
				tmp[k++] = array[i++];
			else
				tmp[k++] = array[j++];
		}
		while (i < mid) {
			tmp[k++] = array[i++];
		}
		while (j < last)
			tmp[k++] = array[j++];
		for (i = 0; i < last - first; ++i)
			array[first + i] = tmp[i];
	}

	public void selectionSort() {
		int i, j;
		for (i = 0; i < array.length; ++i) {
			for (j = i + 1; j < array.length; ++j) {
				if (array[i] > array[j]) {
					int tmp = array[j];
					array[j] = array[i];
					array[i] = tmp;
				}
			}
		}
	}

	public void insertionSort() {
		int j, i;
		for (i = 1; i < array.length; ++i) {
			for (j = i; j > 0; --j) {
				if (array[j - 1] > array[j]) {
					int tmp = array[j];
					array[j] = array[j - 1];
					array[j - 1] = tmp;
				}
			}
		}
	}

	public void printArray() {
		StringBuffer sb = new StringBuffer();
		for (int i = 0; i < array.length; i++) {
			if (i != array.length - 1)
				sb.append(array[i] + ", ");
			else
				sb.append(array[i]);
		}
		System.out.println(sb.toString());
	}

	public void reset() {
		for (int i = 0; i < array.length; ++i) {
			array[i] = orig[i];
		}
		System.out.println("RESETTING ARRAY");
	}
}
