
package com.lib;

public class CircularDoubleArray {
    final double[] dataSet;
    final int capacity;
    int pointer = -1;

    boolean capacityReached = false;

    public CircularDoubleArray(int size) {
        dataSet = new double[size];
        capacity = size;
    }

    public void add(double data) {
        increment(true);
        dataSet[pointer] = data;
    }

    public double getAverage() {
        double avg = 0;
        long total;
        if (capacityReached) {
            for (int i = 0; i < dataSet.length; i++) {
                avg += dataSet[i];
            }
        } else {
            for (int i = 0; i <= pointer; i++) {
                avg += dataSet[i];
            }
        }
        if (capacityReached) {
            total = dataSet.length;
        } else if (pointer > 0) {
            total = pointer + 1; // +1 because the pointer initially is at -1
        } else {
            total = 1;
        }
        return avg / total;
    }

    public String getLRU() {
        StringBuilder sb = new StringBuilder();
        if (pointer >= 0) {
            int initpointer = pointer;
            do {
                increment(false);
                sb.append(dataSet[pointer]);
                sb.append(",");
            } while (initpointer != pointer);
        }

        return sb.toString();
    }

    public String getMRU() {
        StringBuilder sb = new StringBuilder();
        if (pointer >= 0) {
            int initpointer = pointer;
            do {
                sb.append(dataSet[pointer]);
                sb.append(",");
                decrement(false);
            } while (initpointer != pointer);
        }
        return sb.toString();
    }

    private void increment(boolean adding) {
        pointer++;
        if (adding) {
            if (pointer >= capacity) {
                pointer = 0;
                capacityReached = true;
            }
        } else {
            if (pointer >= dataSet.length) {
                pointer = 0;
            }
        }
    }

    private void decrement(boolean adding) {
        pointer--;
        if (pointer < 0) {
            if (adding) {
                pointer = capacity - 1;
            } else {
                pointer = dataSet.length - 1;
            }
        }
    }

    public int getPointer() {
        return pointer;
    }

    public double getHead() {
        if (pointer >= 0) {
            return dataSet[pointer];
        }
        return -1;
    }

    public void clear() {
        pointer = -1;
        capacityReached = false;
    }
}
