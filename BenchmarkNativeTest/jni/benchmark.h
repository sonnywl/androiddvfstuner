/* DO NOT EDIT THIS FILE - it is machine generated */
#include <jni.h>
/* Header for class com_benchmarknative_MainActivity */

#ifndef _Included_com_benchmarknative_MainActivity
#define _Included_com_benchmarknative_MainActivity
#ifdef __cplusplus
extern "C" {
#endif
/*
 * Class:     com_benchmarknative_MainActivity
 * Method:    loadTest
 * Signature: (I)Ljava/lang/String;
 */
JNIEXPORT jstring JNICALL Java_com_benchmarknative_MainActivity_loadTest
  (JNIEnv *, jobject, jint);

#ifdef __cplusplus
}
#endif
#endif
