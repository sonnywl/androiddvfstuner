#include <jni.h>
#include <android/log.h>
#include <stdio.h>
#include <stdlib.h>
#include <math.h>
#include <float.h>
#include <time.h>

#include <benchmark.h>
#include <hello2.h>

#include "basicmath/basicmath_small.h"
#include "basicmath/basicmath_large.h"
#include "bitcount/bitcnts.h"
#include "fft/fft.h"
#include "linpack/linpack.h"

#define  LOG_TAG    "benchmark"
#define  LOGD(...)  __android_log_print(ANDROID_LOG_DEBUG,LOG_TAG,__VA_ARGS__)
#define  LOGI(...)  __android_log_print(ANDROID_LOG_INFO,LOG_TAG,__VA_ARGS__)
#define  LOGW(...)  __android_log_print(ANDROID_LOG_WARN,LOG_TAG,__VA_ARGS__)
#define  LOGE(...)  __android_log_print(ANDROID_LOG_ERROR,LOG_TAG,__VA_ARGS__)

double startSecs;
double secs;
clock_t begin, end;

double getTime() {
	struct timespec tp1;
	clock_gettime(CLOCK_REALTIME, &tp1);
	return (double) tp1.tv_sec + (double) tp1.tv_nsec / 1e9;
}

void start_time() {
	begin = clock();
	return;
}

void end_time() {
	end = clock();
	secs = (double)(end - begin) / CLOCKS_PER_SEC;
	return;
}

JNIEXPORT jstring JNICALL Java_com_benchmarknative_MainActivity_loadTest(
		JNIEnv *env, jobject obj, jint state) {
	char resultchars[1000];
	start_time();
	switch (state) {
	default:
	case 0:
		break;
	case 10: // Basicmath Small
		run_small();
		break;
	case 11: // Basicmath Large
		run_large();
		break;
	case 12: // Linpack
		linpack(2, 200);
		break;
	case 13: // Qsort
		break;
	case 23: // Bitcount Small
		run_bitcnts(75000);
		break;
	case 22: // Bitcount Large
		run_bitcnts(100000);
		break;
	case 24: // Susan
		break;
	case 25: // FFT Small
		run_fft(4, 4096, 0);
		break;
	case 26: // FFT Large
		run_fft(8, 32768, 0);
		break;
	}
	end_time();

	sprintf(resultchars, "%f", secs);
	return (*env)->NewStringUTF(env, resultchars);
}

