LOCAL_PATH:= $(call my-dir)

include $(CLEAR_VARS)

LOCAL_MODULE    := benchmark

LOCAL_CFLAGS    := -Werror
	
LOCAL_C_INCLUDES := hello2.h \
				benchmark.h \
				fft/ddc.h \
				fft/ddcmath.h \
				fft/fourier.h \
				linpack.h 
				
 				
LOCAL_SRC_FILES := hello2.c \
					benchmark.c \
					basicmath/isqrt.c \
					basicmath/cubic.c \
					basicmath/basicmath_small.c \
					basicmath/basicmath_large.c \
					bitcount/bitarray.c \
					bitcount/bitcnt_1.c \
					bitcount/bitcnt_2.c \
					bitcount/bitcnt_3.c \
					bitcount/bitcnt_4.c \
					bitcount/bitfiles.c \
					bitcount/bitstrng.c \
					bitcount/bstr_i.c \
					bitcount/bitcnts.c \
					fft/fourierf.c \
					fft/fftmisc.c \
					fft/fft.c \
					linpack/linpack.c
					
				
LOCAL_LDLIBS    := -llog 

include $(BUILD_SHARED_LIBRARY)	