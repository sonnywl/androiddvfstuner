from com.android.monkeyrunner import MonkeyRunner, MonkeyDevice
from datetime import datetime
import commands
import sys
import os
import time

def touchLocation(device, x, y):
	device.touch(x, y, 'downAndUp')
	MonkeyRunner.sleep(2)
	
def backPress(device):
	# Nexus 4
	touchLocation(device, 125, 1220)
	#device.press('KEYCODE_BACK', MonkeyDevice.DOWN_AND_UP)

def homePress(device):
	touchLocation(device, 350, 1220)
	#device.press('KEYCODE_HOME', MonkeyDevice.DOWN_AND_UP)

def main(argv):
	if len(argv) > 1 :
		type = argv[1]
	else:
		print 'Not enough arguments'
		return
	# starting the application and test
	d = datetime.now()
	file='D:\\Dropbox\\Documents\\workspace\\ProfilerAutomation\\src\\screenshots\\'
	file+=datetime.now().strftime('%Y-%m-%d-%H-%M-%S')+'-'+type+".png"
	startTime = time.time()
	print "Connecting"
	device = MonkeyRunner.waitForConnection(3)
	print "Connected"
	
	if type=="antutu":
		backPress(device)
		MonkeyRunner.sleep(0.5)
		result = device.takeSnapshot()
		result.writeToFile(file, 'png')
		homePress(device)
	elif type=="vellamo":		
		result = device.takeSnapshot()
		result.writeToFile(file, 'png')
		backPress(device)
		backPress(device)
		backPress(device)
		backPress(device)
	elif type=="music":
		touchLocation(device, 380, 1130)
		backPress(device)
		backPress(device)
		backPress(device)
	else:
		result = device.takeSnapshot()
		result.writeToFile(file, 'png')
		backPress(device)
		print "unknown"
	print time.time() - startTime, " seconds"
	print "screen-shot taken and stored on device \n Finishing the test " 

if __name__ == "__main__":
	main(sys.argv)
