from com.android.monkeyrunner import MonkeyRunner, MonkeyDevice
import commands
import sys
import os
import time

def touchLocation(device, x, y):
	device.touch(x, y, 'downAndUp')
	MonkeyRunner.sleep(2)

def main(argv):
	type = "off-profiler"
	startTime = time.time()
	print "Connecting"
	device = MonkeyRunner.waitForConnection()
	print "Connected"
	if len(argv) > 0 :
		type = argv[1]
		print 'got %s' % type

	#TOUCH|{'x':389,'y':1098,'type':'downAndUp',}
	#TOUCH|{'x':324,'y':1250,'type':'downAndUp',}

	
	if type == "on-profiler" :
		device.startActivity(component='com.phoneprofiler/com.profiler.ProfilerMain')
		MonkeyRunner.sleep(1)
		touchLocation(device, 400, 1016) # Start Profiler
		device.press('KEYCODE_HOME', MonkeyDevice.DOWN_AND_UP)
	elif type=="off-profiler":
		device.startActivity(component='com.phoneprofiler/com.profiler.ProfilerMain')
		MonkeyRunner.sleep(1)
		touchLocation(device, 400, 1109) # Stop Profiler
		device.press('KEYCODE_BACK', MonkeyDevice.DOWN_AND_UP)
	elif type=="off-fps":
		device.startActivity(component='com.aatt.fpsm/com.aatt.fpsm.FPSMActivity')
		MonkeyRunner.sleep(1)	
		touchLocation(device, 350, 275) 
		device.press('KEYCODE_BACK', MonkeyDevice.DOWN_AND_UP)
		device.press('KEYCODE_BACK', MonkeyDevice.DOWN_AND_UP)
	elif type=="on-fps":
		device.startActivity(component='com.aatt.fpsm/com.aatt.fpsm.FPSMActivity')
		MonkeyRunner.sleep(1)
		touchLocation(device, 350, 275) 
		device.press('KEYCODE_HOME', MonkeyDevice.DOWN_AND_UP)

	print time.time() - startTime, " seconds"

if __name__ == "__main__":
	main(sys.argv)