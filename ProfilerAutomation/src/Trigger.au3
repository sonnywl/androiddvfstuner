#include <Date.au3>
#include <Array.au3>
Local $GOVERNORS[1] = ["Ondemand"]
Local $TESTS[4] = ["Linpack","Fbreader","benchmarknative", "antutu"]

Local $TESTMAP[5] = ["linpack", "benchmarknative", "fbreader", "antutu", "vellamo"]
Local $TESTMAPTIME[5] = [10, 10, 43, 175, 200]
$testTime = 175
Local $hWnd = WinActivate("Power Tool")

;Local $avArray[10]

;$avArray[0] = "JPM"
;$avArray[1] = "Holger"
;$avArray[2] = "Jon"
;$avArray[3] = "Larry"
;$avArray[4] = "Jeremy"
;$avArray[5] = "Valik"
;$avArray[6] = "Cyberslug"
;$avArray[7] = "Nutster"
;$avArray[8] = "JdeB"
;$avArray[9] = "Tylo"

;_ArrayDisplay($avArray, "$avArray BEFORE _ArrayAdd()")
;_ArrayAdd($avArray, "Brian")
;_ArrayDisplay($avArray, "$avArray AFTER _ArrayAdd()")

;MsgBox(0, "", inData("fbreader", $TESTMAP))

checkParameter()

Func checkParameter()
	ControlClick($hWnd, "", "[NAME:buttonParameters]")
	Sleep(500)
	$tWnd = WinActivate("Set Parameters")
	Sleep(50)
	ControlClick($tWnd, "", "[NAME:buttonGetFactoryDefaults]")
	Sleep(50)
	WinActivate("Set Parameters")
	ControlClick($tWnd, "", "[NAME:buttonApply]")
	Sleep(50)
	WinActivate("Set Parameters")
	ControlClick($tWnd, "", "[NAME:buttonCancel]")
	Sleep(50)
	WinActivate("Set Parameters")
	ControlClick($tWnd, "", "[NAME:buttonCancel]")
	Sleep(50)
EndFunc


Func inData($checkData, $flagArray)
	For $data = 0 To UBound($flagArray) - 1
		If StringCompare($checkData, $flagArray[$data]) == 0 Then
			Return $data
		EndIf
	Next
	Return -1
EndFunc

Func checkTriggerTime($triggerTestTime)
	ControlClick($hWnd, "", "[NAME:buttonSetTrigger]")
	$tWnd = WinActivate("Trigger Settings")
	Send("{TAB}")
	Send("{TAB}")
	Send("{TAB}")
	Send("{TAB}")
	Send("{TAB}")
	Send("{TAB}")
	Send("^C")
	$value = ClipGet()
	If Number($value) <> $testTime Then
		Send($testTime)
	EndIf
	ControlClick($tWnd, "", "[NAME:buttonOk]")
EndFunc


;RunWait("batch\test.bat antutu 123")