from com.android.monkeyrunner import MonkeyRunner, MonkeyDevice
import commands
import sys
import os
import time

from datetime import datetime

def touchLocation(device, x, y):
	device.touch(x, y, 'downAndUp')
	MonkeyRunner.sleep(1)

def main():
	# starting the application and test
	startTime = time.time()
	print "Connecting"
	device = MonkeyRunner.waitForConnection()
	
	print "starting application...."
	device.startActivity(component='com.benchmarknative/com.benchmarknative.MainActivity')
	print "application startup complete"
	print time.time() - startTime, " seconds"

	#Gzip 1246
	touchLocation(device, 155, 994)
	touchLocation(device, 238, 733)
	touchLocation(device, 290, 1104)

	#Gzip 5120
	#touchLocation(device, 240, 1005)
	#touchLocation(device, 245, 826)
	#touchLocation(device, 429, 1117)

	#Gzip 10240
	#touchLocation(device, 146, 1005)
	#touchLocation(device, 137, 930)
	#touchLocation(device, 234, 1114)
	#device.touch(146, 1005, 'downAndUp')
	#device.touch(137, 930, 'downAndUp')
	#device.touch(234, 1114, 'downAndUp')
	MonkeyRunner.sleep(11);

	# Reconnect due to lost
	#device = MonkeyRunner.waitForConnection(1, "0149C2DB0F01800A")
	
	d = datetime.now()
	file='C:\\Users\\sonny\\Dropbox\\Documents\\workspace\\ProfilerAutomation\\screenshots\\'
	file+=datetime.now().strftime('%Y-%m-%d-%H-%M-%S')
	file+='-Benchmark.png'
	result = device.takeSnapshot()
	result.writeToFile(file, 'png')

	print "screenshot taken and stored on device"
	device.press('KEYCODE_BACK', MonkeyDevice.DOWN_AND_UP)
	print "Finishing the test" 

if __name__ == "__main__":
    main()
