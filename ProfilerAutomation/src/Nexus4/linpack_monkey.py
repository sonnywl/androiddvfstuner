from com.android.monkeyrunner import MonkeyRunner, MonkeyDevice
import commands
import sys
import os
import time

print 'Number of arguments:', len(sys.argv), 'arguments.'
print 'Argument List:', str(sys.argv[1])

from datetime import datetime
# starting the application and test
startTime = time.time()
print "Connecting"
device = MonkeyRunner.waitForConnection()

print "starting application...."
device.startActivity(component='com.linpackv7/com.linpackv7.Linpackv7Activity')
print "application startup complete"
print time.time() - startTime, " seconds"

MonkeyRunner.sleep(1)
device.touch(42, 592, 'downAndUp')
MonkeyRunner.sleep(11);

# Reconnect due to lost
#device = MonkeyRunner.waitForConnection()

d = datetime.now()
file='C:\\Users\\sonny\\Dropbox\\Documents\\workspace\\ProfilerAutomation\\screenshots\\'
file+=datetime.now().strftime('%Y-%m-%d-%H-%M-%S')
file+='-Linpack.png'
result = device.takeSnapshot()
result.writeToFile(file, 'png')

print "screenshot taken and stored on device"
device.press('KEYCODE_BACK', MonkeyDevice.DOWN_AND_UP)
print "Finishing the test" 
