from com.android.monkeyrunner import MonkeyRunner, MonkeyDevice
import commands
import sys
import os
import time

def touchLocation(device, x, y):
	device.touch(x, y, 'downAndUp')
	MonkeyRunner.sleep(1.5)

def main(argv):
	type = "off-profiler"
	startTime = time.time()
	print "Connecting"
	device = MonkeyRunner.waitForConnection()
	if len(argv) > 0 :
		type = argv[1]

	#TOUCH|{'x':389,'y':1098,'type':'downAndUp',}
	#TOUCH|{'x':324,'y':1250,'type':'downAndUp',}

	device.startActivity(component='com.phoneprofiler/com.profiler.ProfilerMain')
	if type != "off-profiler" :
		touchLocation(device, 389, 1016) # Start Profiler
		device.press('KEYCODE_HOME', MonkeyDevice.DOWN_AND_UP)
	else:
		touchLocation(device, 389, 1109) # Stop Profiler
		device.press('KEYCODE_BACK', MonkeyDevice.DOWN_AND_UP)
	print time.time() - startTime, " seconds"
	

if __name__ == "__main__":
	main(sys.argv)