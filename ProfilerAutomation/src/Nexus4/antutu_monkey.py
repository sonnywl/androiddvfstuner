from com.android.monkeyrunner import MonkeyRunner, MonkeyDevice
import commands
import sys
import os
import time

from datetime import datetime

def touchLocation(device, x, y):
	device.touch(x, y, 'downAndUp')
	MonkeyRunner.sleep(1.5)

print "Connecting"
# connection to the current device, and return a MonkeyDevice object
device = MonkeyRunner.waitForConnection()

touchLocation(device, 697, 136)
touchLocation(device, 510, 280)
touchLocation(device, 243, 448)
touchLocation(device, 697, 234)

MonkeyRunner.sleep(185);

d = datetime.now()
file='C:\\Users\\sonny\\Dropbox\\Documents\\workspace\\ProfilerAutomation\\screenshots\\'
file+=datetime.now().strftime('%Y-%m-%d-%H-%M-%S')
file+='-AnTuTu.png'
result = device.takeSnapshot()
result.writeToFile(file, 'png')

# sending an event which simulate a click on the menu button
print "Finishing the test" 
