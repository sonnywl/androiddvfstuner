from com.android.monkeyrunner import MonkeyRunner, MonkeyDevice
import commands
import sys
import os
import time

from datetime import datetime
print "Connecting"

# connection to the current device, and return a MonkeyDevice object

device = MonkeyRunner.waitForConnection()

MonkeyRunner.sleep(10);

d = datetime.now()
file='C:\\Users\\sonny\\Dropbox\\Documents\\workspace\\ProfilerAutomation\\screenshots\\'
file+=datetime.now().strftime('%Y-%m-%d-%H-%M-%S')
file+='-Vellamo.png'
result = device.takeSnapshot()
result.writeToFile(file, 'png')
	
MonkeyRunner.sleep(2)
device.press('KEYCODE_BACK', MonkeyDevice.DOWN_AND_UP)
# sending an event which simulate a click on the menu button
device.press('KEYCODE_MENU', MonkeyDevice.DOWN_AND_UP)

print "Finishing the test" 
