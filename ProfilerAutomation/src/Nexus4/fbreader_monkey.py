from com.android.monkeyrunner import MonkeyRunner, MonkeyDevice
import commands
import sys
import os
import time

def touchLocation(device, x, y):
	device.touch(x, y, 'downAndUp')
	MonkeyRunner.sleep(1.5)

# connection to the current device, and return a MonkeyDevice object
startTime = time.time()
print "Connecting"
device = MonkeyRunner.waitForConnection()
print time.time() - startTime, " seconds"
# TOUCH|{'x':351,'y':1240,'type':'downAndUp',}
# TOUCH|{'x':697,'y':136,'type':'downAndUp',}
# TOUCH|{'x':510,'y':280,'type':'downAndUp',}
# TOUCH|{'x':326,'y':520,'type':'downAndUp',}
# TOUCH|{'x':697,'y':234,'type':'downAndUp',}
touchLocation(device, 697, 136)
touchLocation(device, 510, 280)
touchLocation(device, 243, 597)
touchLocation(device, 697, 234)
print time.time() - startTime, " seconds"
print "Finishing the test" 
