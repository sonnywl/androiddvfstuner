from com.android.monkeyrunner import MonkeyRunner, MonkeyDevice
import commands
import sys
import os
import time

def touchLocation(device, x, y):
	device.touch(x, y, 'downAndUp')
	MonkeyRunner.sleep(2)

def backPress(device):
	# Nexus 4
	touchLocation(device, 125, 1220)
	#device.press('KEYCODE_BACK', MonkeyDevice.DOWN_AND_UP)
def homePress(device):
	touchLocation(device, 350, 1220)
	#device.press('KEYCODE_HOME', MonkeyDevice.DOWN_AND_UP)

def star(device):
	touchLocation(device, 736, 162)

def load(device):
	touchLocation(device, 554, 280)

def play(device):
	touchLocation(device, 736, 234) 

def main(argv):
	if len(argv) > 0 :
		type = argv[1]
	else:
		print 'Not Test Argument'
		return
	# starting the application and test
	startTime = time.time()
	print "Connecting"
	device = MonkeyRunner.waitForConnection(3)
	print "Connected"
	homePress(device)
	MonkeyRunner.sleep(1)
	if type=="antutu":
		touchLocation(device, 100, 1095) # 736 162 
		touchLocation(device, 140, 495) # 554 272
		MonkeyRunner.sleep(3)
		touchLocation(device, 204, 642) # Test Again
		touchLocation(device, 345, 469) # Test
	elif type=="vellamo":		
		device.startActivity(component='com.quicinc.vellamo/.VellamoActivity')
		MonkeyRunner.sleep(3)
		touchLocation(device, 391, 416)
	elif type=="linpack":
		device.startActivity(component='com.linpackv7/com.linpackv7.Linpackv7Activity')
		MonkeyRunner.sleep(1)
		touchLocation(device, 45, 666)
	elif type=="linpack-neon":
		device.startActivity(component='com.neonlinpack/com.neonlinpack.NeonLinpackActivity')
		MonkeyRunner.sleep(1)
		touchLocation(device, 45, 666)
	elif type=="linpack-java":
		device.startActivity(component='com.LinpackJava/com.LinpackJava.LinpackJavaActivity')
		MonkeyRunner.sleep(1)
		touchLocation(device, 45, 666)
	elif type=="gzip":
		device.startActivity(component='com.benchmarknative/com.benchmarknative.MainActivity')
		MonkeyRunner.sleep(3)
		#Gzip 1246
		#touchLocation(device, 155, 994)
		#touchLocation(device, 238, 733)
		#touchLocation(device, 290, 1104)
		#Gzip 5120 Galaxy Nexus
		#touchLocation(device, 240, 1005)
		#touchLocation(device, 245, 826)
		#touchLocation(device, 429, 1117)
		#Gzip 5120 Nexus 4
		touchLocation(device, 240, 1000)
		touchLocation(device, 245, 820)
		touchLocation(device, 429, 1090)
		#Gzip 10240
		#touchLocation(device, 146, 1005)
		#touchLocation(device, 137, 930)
		#touchLocation(device, 234, 1114)
	elif type=="fft":
		device.startActivity(component='com.benchmarknative/com.benchmarknative.MainActivity')
		MonkeyRunner.sleep(3)
		touchLocation(device, 240, 1000)
		touchLocation(device, 245, 530)
		touchLocation(device, 429, 1090)
	elif type=="basicmath":
		device.startActivity(component='com.benchmarknative/com.benchmarknative.MainActivity')
		MonkeyRunner.sleep(3)
		touchLocation(device, 240, 1000)
		touchLocation(device, 245, 150)
		touchLocation(device, 429, 1090)
	elif type=="music":
		touchLocation(device, 100, 1095)
		touchLocation(device, 590, 290)
		touchLocation(device, 570, 420)
		touchLocation(device, 350, 590)			
	elif type=="fbreader":
		star(device) # 736 162 
		load(device) # 554 272
		MonkeyRunner.sleep(1)
		touchLocation(device, 400, 666)
		play(device)
	elif type=="angrybird":
		star(device)
		load(device)
		touchLocation(device, 400, 910)
		play(device)
	else:
		print "unknown first param"
		return
	print time.time() - startTime, " seconds"

if __name__ == "__main__":
	main(sys.argv)
