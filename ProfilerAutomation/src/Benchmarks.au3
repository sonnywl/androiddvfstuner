#include <Array.au3>
#include <Date.au3>
#include <File.au3>
#include <FileConstants.au3>
; http://www.anandtech.com/show/7384/state-of-cheating-in-android-benchmarks
; 3DM, AnTuTu, AndEBench, Basemark X, GeekBench 3, GFXB 2.7, Vellamo

; Linpack - 10 secs
; AnTuTu - 190 sec = 3 min 10 sec
; GZip - 10 secs
; Vellamo - 370 sec = 5 min 30 sec
; FBReader - 60 sec
; Fruit Ninja 43 sec
Local Const $MONITORING = 0
Local Const $NUM_ITERATIONS = 2

Local Const $TESTMAP[11] = ["linpack", "linpack-java", "linpack-neon", "gzip", "fft", "basicmath", "fbreader", "antutu", "vellamo", "music", "angrybird"]
; Nexus 4
;Local Const $TESTMAPTIME[UBound($TESTMAP)] = [10, 10, 10, 15, 11, 11, 53, 175, 370, 30, 50]
; Galaxy Nexus
Local Const $TESTMAPTIME[UBound($TESTMAP)] = [10, 10, 10, 15, 11, 11, 53, 190, 370, 30, 50]

Local Const $SCREENSHOT_TESTS[9] = ["linpack", "linpack-java", "linpack-neon", "gzip", "fft", "basicmath", "antutu", "vellamo", "angrybird"]
Local Const $GOVERNORS[3] = ["profiler", "interactive", "ondemand"]
Local Const $TESTS[1] = ["antutu"]

Local Const $GOVS = UBound($GOVERNORS)
Local Const $NUM_TESTS = UBound($TESTS)

Local $TESTS_TIME[0]

; Locate test to get wait time that user estimates manually
For $i = 0 To $NUM_TESTS - 1
	$loc = inData($TESTS[$i], $TESTMAP)
	If $loc >= 0 Then
		_ArrayAdd($TESTS_TIME, $TESTMAPTIME[$loc])
	Else
		MsgBox(0, "", "Something wrong " & $i & " " & $TESTS[$i] & " " & $TESTMAP)
		Exit
	EndIf
Next

Local $hWnd = WinActivate("Power Tool")
Local $sFilePath = "values"
Local $counter = 0

; For Each Governor
For $i = 0 To $GOVS - 1
	If StringCompare($GOVERNORS[$i], "Ondemand") == 0 Then
		RunWait("1-governor_ondemand.bat")
		RunWait("2-governor_ondemand_profiler-off.bat")
		RunWait("1-governor_ondemand_reset.bat")
	ElseIf StringCompare($GOVERNORS[$i], "Profiler") == 0 Then
		RunWait("1-governor_ondemand.bat")
		RunWait("2-governor_ondemand_profiler-on.bat")
	Else
		RunWait("2-governor_ondemand_profiler-off.bat")
		RunWait("0-governor_interactive.bat")
	EndIf

	Local $sFile = $sFilePath & ".txt"
	If Not FileExists($sFile) Then
		createFile($sFile)
	EndIf

	; For Each Test
	For $testCount = 0 To $NUM_TESTS - 1
		checkParameter()
		FileWriteLine($sFile, $GOVERNORS[$i] & " " & $TESTS[$testCount])
		If StringCompare("angrybird", StringLower($TESTS[$testCount]))==0 Then
			RunWait("0-on-fps.bat")
		EndIf
		; For Test Iteration
		While $counter < $NUM_ITERATIONS
			Sleep(1000)
			checkParameter()
			checkTriggerTime($TESTS_TIME[$testCount])
			RunWait("batch\benchmark-start.bat " & StringLower($TESTS[$testCount]))
			$hWnd = WinActivate("Power Tool")
			ControlClick($hWnd, "", "[NAME:buttonRun]")
			Sleep(($TESTS_TIME[$testCount] * 1000))
			WinActivate("Power Tool")
			$consumedEnergy = ControlGetText($hWnd, "", "[NAME:labelStatsConsEnergy]")
			$avgPower = ControlGetText($hWnd, "", "[NAME:labelStatsAvgPower]")
			$avgCurrent = ControlGetText($hWnd, "", "[NAME:labelStatsAvgCur]")
			$avgVoltage = ControlGetText($hWnd, "", "[NAME:labelStatsAvgVout]")
			$expectLife = ControlGetText($hWnd, "", "[NAME:labelBattery]")
			$tCur = _Date_Time_GetLocalTime()

			WinActivate("Power Tool")
			ControlClick($hWnd, "", "[NAME:buttonStop]")
			Sleep(50)
			If $MONITORING == 0 Then
				FileWriteLine($sFile, _Date_Time_SystemTimeToDateTimeStr($tCur) _
						& " " & $consumedEnergy & " " & $avgPower & " " & $expectLife _
						& " " & $avgCurrent & " " & $avgVoltage)
				If inData($TESTS[$testCount], $SCREENSHOT_TESTS) > -1 Then
					RunWait("batch\benchmark-end.bat " & StringLower($TESTS[$testCount]))
				EndIf
			EndIf
			$counter = $counter + 1
		WEnd
		$counter = 0;
		If StringCompare("angrybird", StringLower($TESTS[$testCount]))==0 Then
			RunWait("0-off-fps.bat")
			Sleep(5000)
		EndIf
	Next
Next
RunWait("2-governor_ondemand_profiler-off.bat")
MsgBox(0,"", "Completed")

Func inData($checkData, $flagArray)
	For $data = 0 To UBound($flagArray) - 1
		If StringCompare($checkData, $flagArray[$data]) == 0 Then
			Return $data
		EndIf
	Next
	Return -1
EndFunc

Func createFile($sFilePath)
	If Not FileExists($sFilePath) Then
		If Not _FileCreate($sFilePath) Then
			MsgBox($MB_SYSTEMMODAL, "File Create", _
			"An error occurred whilst writing the temporary file.")
		EndIf
		FileWriteLine($sFilePath, "Time, Energy, Avg Power, Expected Battery, Avg Current, Avg Voltage")
	EndIf
EndFunc

Func checkParameter()
	ControlClick($hWnd, "", "[NAME:buttonParameters]")
	Sleep(500)
	$tWnd = WinActivate("Set Parameters")
	Sleep(50)
	ControlClick($tWnd, "", "[NAME:buttonGetFactoryDefaults]")
	Sleep(50)
	WinActivate("Set Parameters")
	ControlClick($tWnd, "", "[NAME:buttonApply]")
	Sleep(50)
	WinActivate("Set Parameters")
	ControlClick($tWnd, "", "[NAME:buttonCancel]")
	Sleep(50)
	WinActivate("Set Parameters")
	ControlClick($tWnd, "", "[NAME:buttonCancel]")
	Sleep(50)
EndFunc

Func checkTriggerTime($triggerTestTime)
	ControlClick($hWnd, "", "[NAME:buttonSetTrigger]")
	$tWnd = WinActivate("Trigger Settings")
	Sleep(10)
	Send("{TAB}")
	Send("{TAB}")
	Send("{TAB}")
	Send("{TAB}")
	Send("{TAB}")
;	Send("{TAB}")
	Send("^C")
	$value = ClipGet()
	If Number($value) <> $triggerTestTime Then
		Send($triggerTestTime)
	EndIf
	$tWnd = WinActivate("Trigger Settings")
	ControlClick($tWnd, "", "[NAME:buttonOk]")
	Sleep(300)
EndFunc
