#include <Array.au3>
#include <File.au3>
#include <FileConstants.au3>
#include <Date.au3>

; http://www.anandtech.com/show/7384/state-of-cheating-in-android-benchmarks
; 3DM, AnTuTu, AndEBench, Basemark X, GeekBench 3, GFXB 2.7, Vellamo

; Linpack - 10 secs
; AnTuTu - 190 sec = 3 min 10 sec
; GZip - 10 secs
; Vellamo - 330 sec = 5 min 30 sec
; FBReader - 60 sec

; Fruit Ninja 43 sec

; TODO
; 1. Dynamically switchh to profiler
; 2, Batch files for python calling of scripts
; 3. Nexus 4 python locations
; 4. Python timings

;Local $GOVERNORS[1] = ["Interactive"]
Local $GOVERNORS[1] = ["Ondemand"]
;Local $GOVERNORS[1] = ["Profiler"]


;Local $TESTS[3] = ["Linpack","Fbreader","benchmarknative", "mp3", "youtube", "angrybird"]
;Local Const $TESTS_TIME[$sizeTests] = [11000, 43000, 11000, 30000, 38000, 53000]
;Local $TESTS[3] = ["Fbreader","benchmarknative", "mp3", "chromeyoutube", "angrybird"]
;Local Const $TESTS_TIME_NEXUS[$sizeTests] = [11000, 43000, 11000, 30000, 38000, 53000]

Local $TESTS[1] = [ "idle"]
Local Const $sizeTests = UBound($TESTS)
Local Const $TESTS_TIME[$sizeTests] = [30]
Local Const $numTests[$sizeTests] = [6]

Local $hWnd = WinActivate("Power Tool")
Local $sFilePath = "values"
Local $counter = 0

Local $sFile = $sFilePath & ".txt"

If Not FileExists($sFile) Then
	createFile($sFile)
EndIf

For $testCount = 0 To $sizeTests - 1
	FileWriteLine($sFile, $GOVERNORS[0] & " " & $TESTS[$testCount])
	While $counter < $numTests[$testCount]
		$hWnd = WinActivate("Power Tool")
		ControlClick($hWnd, "", "[NAME:buttonRun]")
		Sleep(($TESTS_TIME[$testCount] * 1000)+1000)
		$hWnd = WinActivate("Power Tool")
		$consumedEnergy = ControlGetText($hWnd, "", "[NAME:labelStatsConsEnergy]")
		$avgPower = ControlGetText($hWnd, "", "[NAME:labelStatsAvgPower]")
		$avgCurrent = ControlGetText($hWnd, "", "[NAME:labelStatsAvgCur]")
		$avgVoltage = ControlGetText($hWnd, "", "[NAME:labelStatsAvgVout]")
		$expectLife = ControlGetText($hWnd, "", "[NAME:labelBattery]")
		$tCur = _Date_Time_GetLocalTime()
		FileWriteLine($sFile, _Date_Time_SystemTimeToDateTimeStr($tCur) _
			& " " & $consumedEnergy & " " & $avgPower & " " _
			& $expectLife & " " & $avgCurrent & " " & $avgVoltage)
		$counter = $counter + 1
	WEnd
	$counter = 0;
Next


Func createFile($sFilePath)
	If Not FileExists($sFilePath) Then
		If Not _FileCreate($sFilePath) Then
			MsgBox($MB_SYSTEMMODAL, _
			"File Create", "An error occurred whilst writing the temporary file.")
		EndIf
		FileWriteLine($sFilePath, _
		"Test No, Time, Energy, Avg Power, Expected Battery, Avg Current, Avg Voltage")
	EndIf
EndFunc

Func checkTriggerTime($triggerTestTime)
	ControlClick($hWnd, "", "[NAME:buttonSetTrigger]")
	$tWnd = WinActivate("Trigger Settings")
	Send("{TAB}")
	Send("{TAB}")
	Send("{TAB}")
	Send("{TAB}")
	Send("{TAB}")
	Send("{TAB}")
	Send("^C")
	$value = ClipGet()
	If Number($value) <> $triggerTestTime Then
		Send($triggerTestTime)
	EndIf
	ControlClick($tWnd, "", "[NAME:buttonOk]")
EndFunc

Func inData($checkData, $flagArray)
	For $data = 0 To UBound($flagArray) - 1
		If StringCompare($checkData, $flagArray[$data]) == 0 Then
			Return 1
		EndIf
	Next
	Return 0
EndFunc

