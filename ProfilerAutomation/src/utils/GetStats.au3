#include <Date.au3>
#include <File.au3>
#include <FileConstants.au3>

Local Const $hWnd = WinActivate("Power Tool")
Local $sFilePath = "values.txt";

If $CmdLine[0] > 0 Then
	$sFilePath = $CmdLine[1] & ".txt"
EndIf

If Not FileExists($sFilePath) Then
	createFile($sFilePath)
EndIf

$consumedEnergy = ControlGetText($hWnd, "", "[NAME:labelStatsConsEnergy]")
$avgPower = ControlGetText($hWnd, "", "[NAME:labelStatsAvgPower]")
$avgCurrent = ControlGetText($hWnd, "", "[NAME:labelStatsAvgCur]")
$avgVoltage = ControlGetText($hWnd, "", "[NAME:labelStatsAvgVout]")
$expectLife = ControlGetText($hWnd, "", "[NAME:labelBattery]")
$tCur = _Date_Time_GetLocalTime()

FileWriteLine($sFilePath, _Date_Time_SystemTimeToDateTimeStr($tCur) & " " _
	& $consumedEnergy & " " & $avgPower & " " _
	& $expectLife & " " & $avgCurrent & " " & $avgVoltage)

Func createFile($sFilePath)

	If StringLen($sFilePath) > 0 Then
		$sFile = $sFilePath & ".txt"
	EndIf

	If Not FileExists($sFilePath) Then
		If Not _FileCreate($sFilePath) Then
			MsgBox($MB_SYSTEMMODAL, "File Create", "An error occurred whilst writing the temporary file.")
		EndIf
		FileWriteLine($sFilePath, "Time, Energy, Avg Power, Expected Battery, Avg Current, Avg Voltage")
	EndIf

EndFunc