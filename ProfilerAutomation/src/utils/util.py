from com.android.monkeyrunner import MonkeyRunner, MonkeyDevice
import commands
import sys
import os
import time

from datetime import datetime


def touchLocation(device, x, y):
	device.touch(x, y, 'downAndUp')
	MonkeyRunner.sleep(2)

def main(argv):
	type = "off-profiler"
	startTime = time.time()
	print "Connecting"
	d = datetime.now()
	file='C:\\Users\\sonny\\Dropbox\\Documents\\workspace\\ProfilerAutomation\\src\\screenshots\\'
	file+=datetime.now().strftime('%Y-%m-%d-%H-%M-%S')
	file+='-'+type+".png"
	device = MonkeyRunner.waitForConnection()
	if len(argv) > 0 :
		type = argv[1]
	
	result = device.takeSnapshot()
	result.writeToFile(file, 'png')


if __name__ == "__main__":
	main(sys.argv)