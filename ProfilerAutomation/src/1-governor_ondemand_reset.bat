adb shell "echo 3 > /sys/devices/system/cpu/cpufreq/ondemand/down_differential"
adb shell "echo 0 > /sys/devices/system/cpu/cpufreq/ondemand/ignore_nice_load"
adb shell "echo 1 > /sys/devices/system/cpu/cpufreq/ondemand/io_is_busy"
adb shell "echo 0 > /sys/devices/system/cpu/cpufreq/ondemand/powersave_bias"
adb shell "echo 4 > /sys/devices/system/cpu/cpufreq/ondemand/sampling_down_factor"
adb shell "echo 50000 > /sys/devices/system/cpu/cpufreq/ondemand/sampling_rate"
adb shell "echo 10000 > /sys/devices/system/cpu/cpufreq/ondemand/sampling_rate_min"
adb shell "echo 90 > /sys/devices/system/cpu/cpufreq/ondemand/up_threshold"

adb shell "cat /sys/devices/system/cpu/cpufreq/ondemand/*
